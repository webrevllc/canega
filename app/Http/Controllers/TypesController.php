<?php

namespace App\Http\Controllers;

use App\VesselTypes;
use Illuminate\Http\Request;

use App\Http\Requests;

class TypesController extends Controller
{
    public function index()
    {
        $types = VesselTypes::get();
        return view('admin.types', compact('types'));
    }

    public function edit($id)
    {
        $vesselTypes = VesselTypes::find($id);
        return view('admin.types.edit', compact('vesselTypes'));
    }

    public function create()
    {
        return view('admin.types.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $type = new VesselTypes();
        $type->fill($input);
        $type->save();

        return redirect('/admin/vessel-types');
    }

    public function update($id, Request $request)
    {
        $type = VesselTypes::find($id);
        $input = $request->all();
        $type->fill($input);
        $type->save();

        return redirect('/admin/vessel-types');
    }
    public function destroy($id)
    {
        VesselTypes::destroy($id);
        return redirect()->back();
    }
}
