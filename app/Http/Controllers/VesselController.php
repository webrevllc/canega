<?php

namespace App\Http\Controllers;

use App\Vessel;
use App\VesselTypes;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class VesselController extends Controller
{
    public function index()
    {
        $vessels = Vessel::with('vessel_type')->get();
        return view('admin.home', compact('vessels'));
    }

    public function create()
    {
        $types = VesselTypes::lists('name', 'id');
        return view('admin.vessels.create', compact('types'));
    }

    public function store(Request $request)
    {
        $disk = Storage::disk('s3');
        $input = $request->all();
        $type = VesselTypes::find($input['type']);
        $vessel = new Vessel();
        $vessel->fill($input);
        $vessel->vessel_type()->associate($type);

        if ($request->hasFile('my_pdf')) {
            $pdf = $request->file('my_pdf');
            $original_name = $pdf->getClientOriginalName();
            $name = $vessel->name . "-" . $original_name;
            $path = 'vessels/pdfs/'. $name;
            $disk->put($path, file_get_contents($request->file('my_pdf'), 'public'));
            $vessel->pdf_url =  $this->getFilePathAttribute($path);
        }
        if ($request->hasFile('my_image')) {
            $image = $request->file('my_image');
            $original_name = $image->getClientOriginalName();
            $name = $vessel->name . "-" . $original_name;
            $path = 'vessels/images/'. $name;
            $disk->put($path, file_get_contents($request->file('my_image'), 'public'));
            $vessel->image_url =  $this->getFilePathAttribute($path);
        }
        $vessel->save();
        return redirect('admin/vessels');
    }

    public function edit($id)
    {
        $vessel = Vessel::with('vessel_type')->find($id);
        $types = VesselTypes::lists('name', 'id');
        return view('admin/vessels/edit', compact('vessel', 'types'));
    }

    public function update($id, Request $request)
    {
        $disk = Storage::disk('s3');
        $input = $request->all();
        $type = VesselTypes::find($input['type']);
        $vessel = Vessel::with('vessel_type')->find($id);
        $vessel->update($input);
        $vessel->vessel_type()->associate($type);

        if ($request->hasFile('my_pdf')) {
            $pdf = $request->file('my_pdf');
            $original_name = $pdf->getClientOriginalName();
            $name = $vessel->name . "-" . $original_name;
            $path = 'vessels/pdfs/'. $name;
            $disk->put($path, file_get_contents($request->file('my_pdf'), 'public'));
            $vessel->pdf_url =  $this->getFilePathAttribute($path);
        }
        if ($request->hasFile('my_image')) {
            $image = $request->file('my_image');
            $original_name = $image->getClientOriginalName();
            $name = $vessel->name . "-" . $original_name;
            $path = 'vessels/images/'. $name;
            $disk->put($path, file_get_contents($request->file('my_image'), 'public'));
            $vessel->image_url =  $this->getFilePathAttribute($path);
        }
        $vessel->save();
        return redirect('admin/vessels');
    }

    public function destroy($id)
    {
        Vessel::destroy($id);
        return redirect()->back();
    }


    public function getFilePathAttribute($value)
    {
        $disk = Storage::disk('s3');
        $bucket = Config::get('filesystems.disks.s3.bucket');
        if ($disk->exists($value)) {
            $request = $disk->getDriver()->getAdapter()->getClient()->getObjectUrl($bucket, $value);
            return (string) $request;
        }
        return $value;
    }
}
