<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\Vessel;
use App\VesselTypes;
use Illuminate\Support\Facades\Storage;

Route::group(['web'], function(){

    Route::get('/t', function () {
        $src = Vessel::find(8);
        echo "<img src='$src->image_url'/>";
    });

    Route::get('/', function () {
//        return '<h1 style="text-align: center;margin-top:200px;font-family:Calibri;">Site is down for maintenance</h1>';
        return view('home');
    });

    Route::get('/fleet/{id}', function($id){
        $types = VesselTypes::get();
        $vessels = Vessel::where('vessel_type_id', $id)->get();
        return view('vessel-type', compact('vessels', 'types'));
    });

    Route::get('/about', function () {
        return view('about');
    });
    Route::get('/companies', function () {
        return view('companies');
    });
    Route::get('/certifications', function () {
        return view('certifications');
    });
    Route::get('/pols', function () {
        return view('policiess');
    });
    Route::get('/certifications', function () {
        return view('certifications');
    });
    Route::get('/clients', function () {
        return view('clients');
    });
    Route::get('/company-history', function () {
        return view('company-history');
    });
    Route::get('/locations', function () {
        return view('locations');
    });
    Route::get('/career', function () {
        return view('career');
    });
    Route::get('/services', function () {
        return view('services');
    });

    Route::get('/testimonials', function () {
        return view('testimonials');
    });

    Route::get('/services/pipe', function () {
        return view('/services/pipe');
    });

    Route::get('/services/positioning', function () {
        return view('/services/positioning');
    });

    Route::get('/services/cdi', function () {
        return view('/services/cdi');
    });
    Route::get('/services/chandler', function () {
        return view('/services/chandler');
    });
    Route::get('/services/tankers', function () {
        return view('/services/tankers');
    });
    Route::get('/services/offshore', function () {
        return view('/services/offshore');
    });
    Route::get('/services/diving', function () {
        return view('/services/diving');
    });
    Route::get('/services/rov', function () {
        return view('/services/rov');
    });
    Route::get('/services/inspections', function () {
        return view('/services/inspections');
    });
    Route::get('/services/brokering', function () {
        return view('/services/brokering');
    });
    Route::get('/services/catering', function () {
        return view('/services/catering');
    });
    Route::get('/services/supplychain', function () {
        return view('/services/supplychain');
    });
    Route::get('/services/erp', function () {
        return view('/services/erp');
    });

    Route::post('/contactpost', function(\Illuminate\Http\Request $request){
        $name = $request->name;
        $email = $request->email;
        $subject = $request->subject;
        $note = $request->message;
        Mail::send('contactemail', ['name' => $name, 'email' => $email, 'subject' => $subject, 'note' => $note], function ($m) use ($subject, $email) {
            $m->subject($subject);
            $m->from($email);
            $m->to('canops@canega.com');
        });

        return Redirect::back()->withInput()->with('success', 'Your message was successfully delivered.');
    });

    Route::get('/fleet', function(){
        $mpsv =  Vessel::where('vessel_type_id', 1)->get();
        $psv =  Vessel::where('vessel_type_id', 2)->get();
        $fisv =  Vessel::where('vessel_type_id', 3)->get();
        $spm =  Vessel::where('vessel_type_id', 4)->get();
        return view('fleet', compact('mpsv', 'psv', 'fisv', 'spm'));
    });
    
    Route::get('/contact', function () {
        return view('contact');
    });


    Route::auth();

    Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){
        Route::get('/', function(){
            return redirect('/admin/vessels');
        });

        Route::resource('vessel-types', 'TypesController');
        Route::resource('vessels', 'VesselController');
    });

});



Route::get('sendtest', function(){

    ini_set( 'display_errors', 1 );
    error_reporting( E_ALL );
    $from = "noreply@menparkenterprises.com";
    $to = "srosenthal82@gmail.com";
    $subject = "PHP Mail Test script";
    $message = "This is a test to check the PHP Mail functionality";
    $headers = "From:" . $from;
    dd(mail($to,$subject,$message, $headers));
    echo "Test email sent";
});