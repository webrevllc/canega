<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vessel extends Model
{
    protected $fillable = [
        'name',
        'built',
        'class',
        'crane',
        'mooring',
        'beds',
        'firefighting',
        'loa',
        'speed',
        'clear_deck_area',
        'deck_cargo_load',
        'fresh_water',
        'liquid_mud',
        'fuel_oil',
        'drill_water'
    ];
    public function vessel_type()
    {
        return $this->belongsTo(VesselTypes::class);
    }
}
