<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VesselTypes extends Model
{
    protected $table = 'vessel_types';

    protected $fillable = ['name', 'abbreviation'];

    public function vessels()
    {
        return $this->hasMany(Vessel::class);
    }
}
