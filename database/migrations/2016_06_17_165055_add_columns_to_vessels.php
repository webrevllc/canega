<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToVessels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vessels', function ($table) {
            $table->string('built');
            $table->string('class');
            $table->string('crane');
            $table->string('mooring');
            $table->string('beds');
            $table->string('firefighting');
            $table->dropColumn('features');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
