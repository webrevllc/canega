@extends('layout')

@section('content')


    <section class="inner-banner about">
        <div class="thm-container">
            <h2 class="text-center" style="text-shadow: 1px 2px 4px #000">About Canega</h2>
            <br>
            {{--<ul class="breadcumb">--}}
                {{--<li><a href="/"><i class="fa fa-home"></i> Home</a></li>--}}
                {{--<li><span>About Canega</span></li>--}}
            {{--</ul>--}}
        </div>
    </section>

    {{--<h1 class="text-center">About Canega</h1>--}}
    <section class="featured-services about-page">
        <div class="thm-container">
            <div class="sec-title">
                <h2><span>Who We Are</span></h2><br>
                <h4 class="text-justify">Canega (Casa Negroe Garcia) was founded in Ciudad del Carmen, Campeche, Mexico by Alfonso Esteban Negroe Garcia in 1923 and its privately owned by the Negroe Family. With owned offices and affiliated partners, CANEGA provides a wide range of maritime and offshore oil related services coupled with the highest standards of service, quality and safety throughout Mexico, U.S. Gulf, Caribbean, Central and South America, West Africa and the Middle East.</h4>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-4 paddit bordered">
                    <h2 class="responsiveness">Our Mission</h2>
                    <h4 class="text-justify">Provide our customer with quality, professional and dependable service and deliver 100% customer satisfaction. Superior service is the means by which we achieve customer satisfaction. Underlying this mission is our Total Commitment to Quality Control, Value and Service Reliability. This commitment is carried out through Canega’s Quality Assurance Program. We want to be your ONE-STOP Shipping Company.</h4>
                </div>
                <div class="col-xs-12 col-sm-4 paddit bordered">
                    <h2 class="integrity">Our Vision</h2>
                    <h4 class="text-justify">Provide the maritime, oil & gas industries with a wide range of services, while providing outstanding quality service at every level of our organization. Recognize that quality is more than just meeting our client’s basic needs and requirements; the continuous education and training of our experienced personnel with Quality Service in mind and work as a TEAM with our clients to develop strategic alliances and partnerships.</h4>
                </div>
                <div class="col-xs-12 col-sm-4 paddit ">
                    <h2 class="flexibility">Our Quality Policy</h2>
                    <h4 class="text-justify">Quality Service is the means of achieving and exceeding the required standards to satisfy the needs and to ensure strict adherence to our Clients requirements for a safe, efficient and cost-effective operation. All of our client commitments, supporting actions and services delivered must be recognized as an expression of CANEGA’s image and its Quality System. We pledge to monitor our performance because to CANEGA; “Quality is a continuous never ending commitment to improvement”.</h4>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-3 paddit bordered">
                    <h2 class="integrity">Integrity</h2>
                    <h4 class="text-justify">We have gained our integrity through the trust that clients put in us. We are consistent in our service, methods and principles, thereby earning the respect of our clients. Our vast experience in the industry makes us value the importance of honesty and integrity in a highly competitive marketplace.</h4>
                </div>
                <div class="col-xs-12 col-sm-3 paddit bordered">
                    <h2 class="commitment">Commitment</h2>
                    <h4 class="text-justify">Canega is committed to ensuring our clients receive the highest standards of service from us. From our quality to timely supply services, down to never ending customer focus. We are always there to ensure our relationship with our customer remains a long-lasting one.</h4>
                </div>
                <div class="col-xs-12 col-sm-3 paddit bordered">
                    <h2 class="responsiveness">Responsiveness</h2>
                    <h4 class="text-justify">True to our heritage and bed rock principles, we have extensive experience in the Port and Offshore services industry. We aim to prepare for an eventuality. We operate on a 24/7 basis.</h4>
                </div>
                <div class="col-xs-12 col-sm-3 paddit ">
                    <h2 class="flexibility">Flexibility</h2>
                    <h4 class="text-justify">We understand the ever-changing scenarios that take place in the marine industry. No matter how hard it is, we know there is always a solution anf we will work hard to find it. Our customers are always protected.</h4>
                </div>
            </div>

            <div class="row" style="margin-bottom:50px;">
                <div class="col-md-8 ">
                    <img src="/images/team/delivers-value.png" class="center-block" width="80%"/>
                </div>
                <div class="col-md-3 col-md-offset-1">

                    <div class="call-to-action-text">
                        <style>
                            .lity-content{height:700px;}
                        </style>
                        <a href="/images/team/mexico2014.pdf" data-lity>
                            <img src="images/team/mexico2014.jpg" alt="Service Locations"/>
                        </a>
                        <p><i>Click read our article in the 2014 Mexico Oil & Gas Review</i></p>
                    </div>
                </div>
            </div>
            <div class="row">

            </div>
        </div>
            {{--<div class="row">--}}
                {{--<div class="col-md-4 hidden-sm">--}}
                    {{--<section class="faq-section" style="padding-top:40px;">--}}
                        {{--<div class="thm-container">--}}
                            {{--<div class="row">--}}
                                {{--<div class="">--}}
                                    {{--<div class="accrodion-grp" data-grp-name="faq-accrodion">--}}
                                        {{--<div class="accrodion active">--}}
                                            {{--<div class="accrodion-title">--}}
                                                {{--<h4 class="text-justify">Our Mission</h4>--}}
                                            {{--</div>--}}
                                            {{--<div class="accrodion-content">--}}
                                                {{--<div class="img-caption">--}}
                                                    {{--<div class="content-box">--}}
                                                        {{--<h4 class="text-justify">Quality Service is #1</h4>--}}
                                                        {{--<p>Provide our customer with quality, professional and dependable service and deliver 100% customer satisfaction. Superior service is the means by which we achieve customer satisfaction. Underlying this mission is our Total Commitment to Quality Control, Value and Service Reliability. This commitment is carried out through Canega’s Quality Assurance Program. We want to be your ONE-STOP Shipping Company.</p>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="accrodion ">--}}
                                            {{--<div class="accrodion-title">--}}
                                                {{--<h4 class="text-justify">Our Vision</h4>--}}
                                            {{--</div>--}}
                                            {{--<div class="accrodion-content">--}}
                                                {{--<div class="img-caption">--}}
                                                    {{--<div class="content-box">--}}
                                                        {{--<h4 class="text-justify">Record of Proof</h4>--}}
                                                        {{--<p>Provide the maritime, oil & gas industries with a wide range of services, while providing outstanding quality service at every level of our organization. Recognize that quality is more than just meeting our client’s basic needs and requirements; the continuous education and training of our experienced personnel with Quality Service in mind and work as a TEAM with our clients to develop strategic alliances and partnerships.</p>--}}
                                                        {{--<ul>--}}
                                                            {{--• Provide Oil & Maritime Industries with a wide-range of services<br>--}}
                                                            {{--• Provide Quality Service at every level of our Organization<br>--}}
                                                            {{--• Recognize that Quality is more than just meeting our client’s--}}
                                                            {{--basic needs and requirements<br>--}}
                                                            {{--• Continuous education of personnel with Quality Service in mind<br>--}}
                                                            {{--• Work as a TEAM with our Clients to develop Partnerships and--}}
                                                            {{--Strategic Alliances<br>--}}

                                                        {{--</ul>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="accrodion">--}}
                                            {{--<div class="accrodion-title">--}}
                                                {{--<h4 class="text-justify">Our Quality Policy</h4>--}}
                                            {{--</div>--}}
                                            {{--<div class="accrodion-content">--}}
                                                {{--<div class="img-caption">--}}
                                                    {{--<div class="content-box">--}}
                                                        {{--<h4 class="text-justify">Our Policy</h4>--}}
                                                        {{--<p>Quality Service is the means of achieving and exceeding the required standards to satisfy the needs and to ensure strict adherence to our Clients requirements for a safe, efficient and cost-effective operation. All of our client commitments, supporting actions and services delivered must be recognized as an expression of CANEGA’s image and its Quality System. We pledge to monitor our performance because to CANEGA; “Quality is a continuous never ending commitment to improvement”.</p>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<br>--}}
                                {{--<a href="/locations"  class="thm-btn">view our locations <i class="fa fa-arrow-circle-right"></i></a>--}}
                            {{--</div>--}}

                        {{--</div>--}}

                    {{--</section>--}}
                {{--</div>--}}
                {{--<div class="col-md-8 featured-service-box">--}}
                    {{--<div class="col-xs-12 col-sm-6">--}}
                        {{--<div class="single-featured-service">--}}
                            {{--<header>--}}
                                {{--<img src="images/flags2/mexican.png" alt="" width="15%" class="center-block">--}}
                                {{--<h3 class="text-center"><span>Canega S.A. de C.V.</span></h3>--}}
                            {{--</header>--}}
                            {{--<article>--}}
                                {{--<h4 class="text-justify">With Headquarters in Cd. del Carmen, Campeche, Mexico, this member of Canega Group provides a wide-range of Port Agency Services in Mexico.</h4>--}}
                            {{--</article>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-12 col-sm-6">--}}
                        {{--<div class="single-featured-service">--}}
                            {{--<header>--}}
                                {{--<img src="images/flags2/american.png" alt="" width="15%" class="center-block">--}}
                                {{--<h3 class="text-center"><span>Canega Shipping Services</span></h3>--}}
                            {{--</header>--}}
                            {{--<article>--}}
                                {{--<h4 class="text-justify">Established in 1983, this company serves as the Headquarters of Canega Group in the United States.</h4>--}}
                            {{--</article>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="col-xs-12 col-sm-6">--}}
                        {{--<div class="single-featured-service">--}}
                            {{--<header>--}}
                                {{--<img src="images/flags2/nv.png" alt="" width="15%" class="center-block">--}}
                                {{--<h3 class="text-center"><span>Canega Shipping Services N.V.</span></h3>--}}
                            {{--</header>--}}
                            {{--<article>--}}
                                {{--<h4 class="text-justify">The only fully independent Port Agency with a Certified Quality Assurance program in St. Eustatius, Dutch Caribbean.</h4>--}}
                            {{--</article>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-12 col-sm-6">--}}
                        {{--<div class="single-featured-service">--}}
                            {{--<header>--}}
                                {{--<img src="images/flags2/mexican.png" alt="" width="15%" class="center-block">--}}
                                {{--<h3 class="text-center"><span>Canega Internacional S.A. de C.V.</span></h3>--}}
                            {{--</header>--}}
                            {{--<article>--}}
                                {{--<h4 class="text-justify">This member of Canega Group is the Ship Owner, Time Charterer and Operator of Mexican flag offshore vessels.</h4>--}}
                            {{--</article>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="col-xs-12 col-sm-6">--}}
                        {{--<div class="single-featured-service">--}}
                            {{--<header>--}}
                                {{--<img src="images/flags2/american.png" alt="" width="15%" class="center-block">--}}
                                {{--<h3 class="text-center"><span>Canega Offshore Services</span></h3>--}}
                            {{--</header>--}}
                            {{--<article>--}}
                                {{--<h4 class="text-justify">This company is the Chartering, Brokering and Sales Arm of Canega Group.</h4>--}}
                            {{--</article>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-12 col-sm-6">--}}
                        {{--<div class="single-featured-service">--}}
                            {{--<header>--}}
                                {{--<img src="images/flags2/uae.png" alt="" width="15%" class="center-block">--}}
                                {{--<h3 class="text-center"><span>Canega Trading FZE</span></h3>--}}
                            {{--</header>--}}
                            {{--<article>--}}
                                {{--<h4 class="text-justify">This company is was established in the UAE to expand Canega’s operations into the Arabian Gulf, Mediterranean and West Africa.</h4>--}}
                            {{--</article>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="col-xs-12 col-sm-6 col-sm-offset-3">--}}
                        {{--<div class="single-featured-service">--}}
                            {{--<header>--}}
                                {{--<img src="images/flags2/singapore.png" alt="" width="15%" class="center-block">--}}
                                {{--<h3 class="text-center"><span>Canega International, PTE LTD</span></h3>--}}
                            {{--</header>--}}
                            {{--<article>--}}
                                {{--<h4 class="text-justify">This company is was established in the UAE to expand Canega’s operations into the Arabian Gulf, Mediterranean and West Africa.</h4>--}}
                            {{--</article>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}


            {{--</div>--}}
        </div>
    </section>

    @include('partials.clients')

@endsection