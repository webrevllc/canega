@extends('layout')

@section('content')

    <div class="container" >
        <div class="row">
            <h2><a href="{{route('admin.vessels.create')}}" class="thm-btn pull-right">Add a New Vessel <i class="fa fa-arrow-circle-right"></i> </a></h2>
        </div>
        <div class="row" >
            <div class="col-md-3 pull-left">
                <div class="single-sidebar-widget">
                    <div class="special-links">
                        <ul>
                            <ul>
                                <li><a href="/admin/vessels">Vessels</a></li>
                                <li><a href="/admin/vessel-types">Vessel Types</a></li>
                            </ul>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9 pull-right">
                <br>
                <br>
                <table class="table table-bordered">
                    <thead>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Built</th>
                        <th>Class</th>
                        {{--<th>Crane</th>--}}
                        {{--<th>4 PT Mooring</th>--}}
                        {{--<th>Beds</th>--}}
                        {{--<th>Fire Fighting</th>--}}
                        {{--<th>LOA</th>--}}
                        {{--<th>Speed</th>--}}
                        {{--<th>Pax</th>--}}
                        <th>Type</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        @foreach($vessels as $v)
                            <tr>

                                <td>
                                    <h4>{{$v->name}}</h4>
                                    <br>
                                    @if($v->pdf_url != '')
                                        <a href="{{$v->pdf_url}}" class="btn btn-primary" target="_blank"><i class=" fa fa-eye"></i> View PDF</a>
                                    @endif
                                </td>
                                <td>
                                    @if($v->image_url != '')
                                        <img src="{{$v->image_url}}" style="max-width: 70px;" height="70px" class="center-block"/>
                                    @endif
                                </td>
                                <td>{{$v->built}}</td>
                                <td>{{$v->class}}</td>
                                {{--<td>{{$v->crane}}</td>--}}
                                {{--<td>{{$v->mooring}}</td>--}}
                                {{--<td>{{$v->beds}}</td>--}}
                                {{--<td>{{$v->firefighting}}</td>--}}
                                {{--<td>{{$v->loa}}</td>--}}
                                {{--<td>{{$v->speed}}</td>--}}
                                {{--<td>{{$v->pax}}</td>--}}
                                <td>{{$v->vessel_type->abbreviation}}</td>
                                <td>
                                    <a style="margin-bottom:5px;" href="{{route('admin.vessels.edit', ['id' => $v->id])}}" class="btn btn-block btn-info">Edit Vessel</a>

                                    <form action="{{ route('admin.vessels.destroy', ['id' => $v->id]) }}" method="POST">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button class="btn btn-block btn-danger" type="submit">Delete Vessel</button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    </div>

@endsection

