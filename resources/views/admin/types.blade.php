@extends('layout')

@section('content')

    <div class="container">
        <div class="row">
            <h2><a href="{{route('admin.vessel-types.create')}}" class="thm-btn pull-right">Add a New Type <i class="fa fa-arrow-circle-right"></i> </a></h2>
            <div class="col-md-4 pull-left">
                <div class="single-sidebar-widget">
                    <div class="special-links">
                        <ul>
                            <ul>
                                <li><a href="/admin/vessels">Vessels</a></li>
                                <li><a href="/admin/vessel-types">Vessel Types</a></li>
                            </ul>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8 pull-right">
                <h2>Vessel Types</h2>
                <table class="table table-bordered">
                    <thead>
                        <th>Name</th>
                        <th>Abbreviation</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        @foreach($types as $t)
                            <tr>
                                <td>{{$t->name}}</td>
                                <td>{{$t->abbreviation}}</td>
                                <td>
                                    <a style="margin-bottom:5px;" href="{{route('admin.vessel-types.edit', ['id' => $t->id])}}" class="btn btn-block btn-info">Edit Type</a>

                                    <form action="{{ route('admin.vessel-types.destroy', ['id' => $t->id]) }}" method="POST">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button class="btn btn-block btn-danger" type="submit">Delete Type</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    </div>



@endsection