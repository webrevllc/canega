@extends('layout')

@section('content')

    <div class="container" >
        <h2>Add a New Vessel Type</h2>
        <form action="{{route('admin.vessel-types.store')}}" method="post" >
            {{csrf_field()}}
                <div class="form-group">
                    <label >Name</label>
                    <input type="text" class="form-control" name="name" />
                </div>
                <div class="form-group">
                    <label>Abbreviation</label>
                    <input type="text" class="form-control" name="abbreviation"/>
                </div>

            <button type="submit" class="btn btn-lg btn-primary pull-right">Submit</button>

        </form>

    </div>



@endsection