@extends('layout')

@section('content')

    <div class="container">
        <h2>Edit {{$vesselTypes->name}}</h2>
        <form action="{{route('admin.vessel-types.update', $vesselTypes->id)}}" method="post">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="PUT">
            <div class="form-group">
                <label >Name</label>
                <input type="text" class="form-control" name="name" value="{{$vesselTypes->name}}"/>
            </div>
            <div class="form-group">
                <label>Abbreviation</label>
                <input type="text" class="form-control" name="abbreviation" value="{{$vesselTypes->abbreviation}}"/>
            </div>

            <button type="submit" class="btn btn-lg btn-primary pull-right">Submit</button>
            <div class="clearfix">  </div>
            <br>
        </form>

    </div>

@endsection

