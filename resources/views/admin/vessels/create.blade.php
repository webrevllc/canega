@extends('layout')

@section('content')

    <div class="container" >
        <h2>Add a New Vessel</h2>
        <div ng-controller="createController">
            <form action="{{route('admin.vessels.store')}}" method="post" enctype="multipart/form-data" >
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Type</label>
                            {!! Form::select('type', $types, null, ['class' => 'form-control', 'ng-model' => 'type']) !!}
                        </div>
                        <div class="form-group">
                            <label >Vessel Name</label>
                            <input type="text" class="form-control" name="name" />
                        </div>
                        <div class="form-group">
                            <label>Built</label>
                            <input type="text" class="form-control" name="built"/>
                        </div>
                        <div class="form-group">
                            <label>Class</label>
                            <input type="text" class="form-control" name="class"/>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group" ng-if="type==1 || type==2">
                            <label>Crane</label>
                            <input type="text" class="form-control" name="crane"/>
                        </div>
                        <div class="form-group" ng-if="type==1 || type==2">
                            <label>4 PT Mooring</label>
                            {!! Form::select('mooring', ['Yes' => 'Yes', 'No' => 'No'], null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group" ng-if="type==1 || type==2">
                            <label>Beds</label>
                            <input type="text" class="form-control" name="beds"/>
                        </div>
                        <div class="form-group" ng-if="type!=1">
                            <label>LOA</label>
                            <input type="text" class="form-control" name="loa"/>
                        </div>
                        <div class="form-group" ng-if="type==3 || type==4">
                            <label>Speed</label>
                            <input type="text" class="form-control" name="speed"/>
                        </div>
                        <div class="form-group" ng-if="type==3 || type==4">
                            <label>Pax</label>
                            <input type="text" class="form-control" name="pax"/>
                        </div>
                        <div class="form-group" ng-if="type==3">
                            <label>Clear Deck Space</label>
                            <input type="text" class="form-control" name="clear_deck_space"/>
                        </div>
                        <div class="form-group" ng-if="type==3">
                            <label>Deck Cargo Capacity</label>
                            <input type="text" class="form-control" name="deck_cargo_capacity"/>
                        </div>

                        <div class="form-group">
                            <label>Fire Fighting</label>
                            <input type="text" class="form-control" name="firefighting"/>
                        </div>

                    </div>
                </div>
                <div class="row" ng-if="type==2">
                    <div class="col-md-12">
                        <h3>PSV Specific</h3>
                        <div class="form-group">
                            <label>Clear Deck Area</label>
                            <input type="text" class="form-control" name="clear_deck_area"/>
                        </div>
                        <div class="form-group">
                            <label>Deck Cargo Load</label>
                            <input type="text" class="form-control" name="deck_cargo_load"/>
                        </div>
                        <div class="form-group">
                            <label>Potable/Fresh Water</label>
                            <input type="text" class="form-control" name="fresh_water"/>
                        </div>
                        <div class="form-group">
                            <label>Liquid Mud</label>
                            <input type="text" class="form-control" name="liquid_mud"/>
                        </div>
                        <div class="form-group">
                            <label>Fuel  Oil</label>
                            <input type="text" class="form-control" name="fuel_oil"/>
                        </div>
                        <div class="form-group">
                            <label>Drill Water</label>
                            <input type="text" class="form-control" name="drill_water"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Upload PDF</label>
                            <input type="file" name="my_pdf">
                        </div>
                        <div class="form-group">
                            <label>Upload Image</label>
                            <input type="file" name="my_image">
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-lg btn-primary pull-right">Submit</button>
                <div class="clearfix"></div>
                <br>
            </form>
        </div>

    </div>
@endsection

@section('footer')
    <script src="/js/angular.js"></script>
    <script>
        var myApp = angular.module('myApp', [], function($interpolateProvider) {
            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');
        });

        myApp.controller('createController', ['$scope', function($scope){

        }]);

    </script>
@endsection