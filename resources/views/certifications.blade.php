@extends('layout')

@section('content')


    <section class="inner-banner about">
        <div class="thm-container">
            <h2 class="text-center" style="text-shadow: 1px 2px 4px #000">Certifications<br>&amp; Memberships</h2>
            {{--<ul class="breadcumb">--}}
                {{--<li><a href="/"><i class="fa fa-home"></i> Home</a></li>--}}
                {{--<li><span>Our Policies</span></li>--}}
            {{--</ul>--}}
        </div>
    </section>

    <section class=" page-title">
        <div class="thm-container" style="min-height: 450px;">

            <br>
            {{--<div class="">--}}
                {{--<div class="sec-title">--}}
                    {{--<h2><span>Policies</span></h2>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="">--}}
                {{--<table class="table table-bordered">--}}
                    {{--<tr>--}}
                        {{--<td class="col-md-10"><h3>Code of Conduct</h3></td>--}}
                        {{--<td class="col-md-2"><a href="/assets/coc.pdf" class="btn btn-block btn-info" target="_blank"><i class="fa fa-eye"></i>   View</a></td>--}}
                    {{--</tr>--}}
                {{--</table>--}}
            {{--</div>--}}
            {{--<br>--}}
            {{--<br>--}}
            {{--<br>--}}
            <div class="">
                <div class="sec-title">
                    <h2><span>Certifications</span></h2>
                </div>
            </div>

            <div class="">
                <table class="table table-bordered">
                    <tr>
                        <td class="col-md-1"><img src="/images/certifications/trace.png" width="100%" class="center-block"/></td>
                        <td class="col-md-10" style="vertical-align: middle;"><h3>The internationally recognized organization
                                working with companies to raise anti-bribery
                                compliance standards worldwide.
                            </h3></td>
                        <td class="col-md-1"><a href="/assets/trace.pdf" style="margin-top:15px;" class="btn btn-block btn-info" target="_blank"><i class="fa fa-eye"></i>   View</a></td>
                    </tr>
                    <tr>
                        <td class="col-md-1"><img src="/images/certifications/isn.png" width="100%" class="center-block"/></td>
                        <td class="col-md-10" style="vertical-align: middle;">
                            <h3>
                                ISNetworld - Mexico – Global Leader Resource for helping connect qualified contractors and suppliers with hiring clients around the globe.
                            </h3>
                        </td>
                        <td class="col-md-1"><a href="/pdfs/certificates/ISNetWorld-Mexico.pdf" style="margin-top:15px;" class="btn btn-block btn-info" target="_blank"><i class="fa fa-eye"></i>   View</a></td>
                    </tr>
                </table>
            </div>
            <br>
            <br>
            <br>
            <div class="">
                <div class="sec-title">
                    <h2><span>Memberships</span></h2>
                </div>
            </div>

            <div class="">
                <table class="table table-bordered">
                    {{--<tr>--}}
                        {{--<td class="col-md-1"><img src="/images/certifications/bimco.jpg" width="100%" class="center-block"/></td>--}}
                        {{--<td class="col-md-3"><h3>BIMCO</h3></td>--}}
                        {{--<td class="col-md-8">BIMCO is the world’s largest international shipping association, with more than 2,200 members globally. We provide a wide range of services to our global membership – which includes shipowners, operators, managers, brokers and agents. </td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td class="col-md-2"><img src="/images/certifications/amanac.jpg" width="100%" class="center-block"/></td>
                        {{--<td class="col-md-2"><h3>AMANAC</h3></td>--}}
                        <td class="col-md-9" style="vertical-align: middle;"><h3>We are a proud member of Mexican Association of Shipping Agents A.C.</h3></td>
                        <td class="col-md-1"><a href="http://www.amanac.org.mx/" class="btn btn-block btn-info" style="margin-top:10px;" target="_blank"><i class="fa fa-eye"></i>   View</a></td>
                    </tr>
                    <tr>
                        <td class="col-md-2"><img src="/images/fonasba.jpg" height="60px" class="center-block"/></td>
                        {{--<td class="col-md-2"><h3>AMANAC</h3></td>--}}
                        <td class="col-md-9" style="vertical-align: middle;"><h3>Members of The Federation of National Associations of Ship Brokers and Agents</h3></td>
                        <td class="col-md-1"><a href="https://www.fonasba.com/" class="btn btn-block btn-info" style="margin-top:10px;" target="_blank"><i class="fa fa-eye"></i>   View</a></td>
                    </tr>
                </table>
            </div>
            <br><br><br>
                {{--<h4><b>CANEGA GROUP hereby adopts the following Code of Conduct with respect to all commercial transactions, whether local or international: </b></h4>--}}
                {{--<br>--}}
                {{--<h4 class="text-justify"><b>LOCAL AND FOREIGN LAWS:</b>  Neither CANEGA GROUP, nor anyone acting on behalf of CANEGA GROUP, may, directly or indirectly, break or seek to evade the laws or regulations of any country in, through, or with which CANEGA GROUP seeks to do business. That an illegal act is a “customary business practice” in any country is not sufficient justification for violation of this provision. </h4>--}}
                {{--<br>--}}
                {{--<h4 class="text-justify"><b>BRIBERY and FACILITATING PAYMENTS:</b>  Neither CANEGA GROUP, nor anyone acting on behalf of CANEGA GROUP, may, directly or indirectly, offer or provide a bribe, and all demands for bribes must be expressly rejected. </h4>--}}
                {{--<br>--}}
                {{--<h4 class="text-justify">Bribery includes any offer, promise, or gift of any pecuniary or other advantage, whether directly or through intermediaries, to a public official, political party, political candidate or party official or any private sector employee, in order that the official or employee act or refrain from acting in relation to the performance of their duties, in order to obtain or retain business or other business advantage.--}}
                    {{--Neither CANEGA GROUP, nor anyone acting on behalf of CANEGA GROUP, shall offer or make facilitating payments to government officials in order to encourage them to expedite a routine governmental task that they are otherwise required to undertake.  CANEGA GROUP, or anyone acting on behalf of CANEGA GROUP, shall have discretion to deviate from this prohibition if he/she believes that there is an immediate threat to his/her or another’s health or safety.  The circumstances of such payment must be reported as soon as possible after the event and the payment properly recorded. CANEGA GROUP recognizes that extortion is widespread and that participation by the business community increases demand for facilitating payments.--}}
                {{--</h4>--}}
                {{--<br>--}}
                {{--<h4 class="text-justify">Neither CANEGA GROUP, nor anyone acting on behalf of CANEGA GROUP, shall offer or make facilitating payments to government officials in order to encourage them to expedite a routine governmental task that they are otherwise required to undertake.  CANEGA GROUP, or anyone acting on behalf of CANEGA GROUP, shall have discretion to deviate from this prohibition if he/she believes that there is an immediate threat to his/her or another’s health or safety.  The circumstances of such payment must be reported as soon as possible after the event and the payment properly recorded. CANEGA GROUP recognizes that extortion is widespread and that participation by the business community increases demand for facilitating payments. </h4>--}}
                {{--<br>--}}
                {{--<h4 class="text-justify"><b>KICK-BACKS:</b> Neither CANEGA GROUP, nor anyone acting on behalf of CANEGA GROUP, may offer or accept a “kick-back” of any portion of a contract payment to employees of other parties to a contract or use other vehicles such as subcontracts, purchase orders or consulting agreements to channel payments to government officials, political candidates, employees of other parties to a contract, their relatives or business associates.  </h4>--}}
                {{--<br>--}}
                {{--<h4 class="text-justify">A “kickback” is a particular form of bribe which takes place when a person entrusted by an employer or public function has some responsibility for the granting of a benefit and does so in a way that secures a return (kickback) of some of the value of that transaction or benefit for that person without the knowledge or authorization of the employer or public body to which the person is accountable. </h4>--}}
                {{--<br>--}}

                {{--<h4 class="text-justify"><b>CONFLICTS OF INTEREST:</b>  CANEGA GROUP, and anyone acting on behalf of CANEGA GROUP, shall avoid any relationship or activity that might impair, or appear to impair, the ability to render objective and appropriate business decisions in the performance of our jobs</h4>--}}
                {{--<br>--}}
                {{--<h4 class="text-justify"><b>POLITICAL CONTRIBUTIONS:</b>  Neither CANEGA GROUP, nor anyone acting on behalf of CANEGA GROUP, may make a political contribution in order to obtain an unlawful business advantage.  CANEGA GROUP shall comply with all public disclosure requirements. </b></h4>--}}
                {{--<br>--}}
                {{--<h4 class="text-justify"><b>PHILANTHROPIC CONTRIBUTIONS:</b>  CANEGA GROUP, and anyone acting on behalf of CANEGA GROUP, may make contributions only for bona fide charitable purposes and only where permitted by the laws of the country in which the contribution is made.  Contributions made in order to obtain an unlawful business advantage are prohibited. </b></h4>--}}
                {{--<br>--}}
                {{--<h4 class="text-justify"><b>EXTORTION:</b> CANEGA GROUP, and anyone acting on behalf of CANEGA GROUP, shall reject any direct or indirect request by a public official, political party, party official, or private sector employee for undue pecuniary or other advantage, to act or refrain from acting in relation to his or her duties. </h4>--}}
                {{--<br>--}}
                {{--<h4 class="text-justify"><b>GIFTS, HOSPITALITY AND ENTERTAINMENT:</b> CANEGA GROUP, and anyone acting on behalf of CANEGA GROUP, shall avoid the offer or receipt of gifts, meals, entertainment, hospitality or payment of expenses whenever these could materially affect the outcome of business transactions, are not reasonable and bona fide expenditures, or are in violation of the laws of the country of the recipient.  </b></h4>--}}
                {{--<br>--}}
                {{--<h4 class="text-justify"><b>REPORTING REQUIREMENT:  </b>All officers and employees of CANEGA GROUP and anyone acting on behalf of CANEGA GROUP shall promptly report any actual or potential violation of this Code of Conduct, including any instance in which he/she is subjected to any form of extortion or is asked to participate in any way in a bribery scheme, to CANEGA GROUP senior corporate management, without fear that his/her business relationship or employment will be adversely affected.  Reports shall be treated confidentially to the extent possible, consistent with the need to conduct a thorough investigation. </h4>--}}
                {{--<br>--}}
                {{--<h4 class="text-justify"><b>COMPANY RESPONSE:  </b>No employee will suffer demotion, penalty or other adverse consequences for not paying bribes even when CANEGA GROUP may lose business as a result of the employee’s refusal to do so.  Employees are required to report alleged violations of this Code of Conduct to senior management and no employee will suffer demotion, penalty or adverse consequences for reporting.  CANEGA GROUP shall, where appropriate, sanction employees, suppliers or other business partners for violations of this Code of Conduct. </h4>--}}
                {{--<br>--}}
                {{--<h4 class="text-justify"><b>ACCOUNTS:  </b>CANEGA GROUP shall maintain complete and accurate financial records, ensuring that all transactions are properly, accurately and fairly recorded in a single set of books. </h4>--}}
                {{--<br>--}}
                {{--<h4 class="text-justify"><b>COMMUNICATIONS AND TRAINING:  </b>CANEGA GROUP agrees to participate in anti-corruption training provided by TRACE, or by a comparable organization, and to make annual training available for all principals and for all key employees involved in sales, marketing, and procurement.</h4>--}}
                {{--<br>--}}
                {{--<h4 class="text-justify">The person whose signature appears below is duly authorized to adopt this Code of Conduct on behalf of CANEGA GROUP and, if signing on behalf of a company, agrees that this Code shall apply to all officers, employees and representatives of CANEGA GROUP.</h4>--}}
                {{--<br>--}}
                {{--<h4 class="text-justify">We hereby adopt and agree to comply with the Code of Conduct as outlined above.  </h4>--}}
            {{--<br>--}}
            {{--<br>--}}
            {{--</div>--}}
        </div>
    </section>





@endsection