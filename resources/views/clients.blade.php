@extends('layout')
@section('header')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.css"/>
@endsection
@section('content')

    <section class="inner-banner about">
        <div class="thm-container">
            <h2 class="text-center" style="text-shadow: 1px 2px 4px #000">Our Clients & <br>Chartering Record</h2>
            {{--<ul class="breadcumb">--}}
                {{--<li><a href="index"><i class="fa fa-home"></i> Home</a></li>--}}
                {{--<li><span>Our Clients & Chartering Record</span></li>--}}
            {{--</ul>--}}
        </div>
    </section>

    <section class="sec-pasdding">
        <div class="thm-container">
            <div class="row" style="margin-top:30px;">
                <div class="col-sm-12">
                    <table class="table table-striped" id="myTable" >
                        <thead>
                            <th class="col-md-3">Company</th>
                            <th class="col-md-3">Contract Period</th>
                            <th class="col-md-6">Vessels/Unit Chartered/Operated</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>European Marine Contractors</td>
                            <td class="text-center">1998-2000</td>
                            <td>Seamac 1, Castoro 10, Bar Protector</td>
                        </tr>
                        <tr>
                            <td>Schlumberger/Geco Prakla</td>
                            <td class="text-center">1999-2006</td>
                            <td>Geco Diamon, Geco Topaz, Geco Dolphin, Geco Manta</td>
                        </tr>
                        <tr>
                            <td>I.P.C.</td>
                            <td class="text-center">1999-2001</td>
                            <td>Discovery 1</td>
                        </tr>
                        <tr>
                            <td>Sonat Drilling</td>
                            <td class="text-center">1992-1998</td>
                            <td>Dual Rig 84, Dual Rig 87, Discovery 511</td>
                        </tr>
                        <tr>
                            <td>Nabors Drilling</td>
                            <td class="text-center">2002-2012</td>
                            <td>Sundawell 1, Nabors 659</td>
                        </tr>
                        <tr>
                            <td>PGS</td>
                            <td class="text-center">2000-2003</td>
                            <td>Betty Chouest, Elda Chouest, Ranform Explorer, Kondor Explorer</td>
                        </tr>
                        <tr>
                            <td>Edison Chouest Offshore</td>
                            <td class="text-center">2000-2002</td>
                            <td>Fast Solution, Eddie Chouest, Eastern Spirit</td>
                        </tr>
                        <tr>
                            <td>Stolt Comex Seaway Inc.</td>
                            <td class="text-center">2002- 2004</td>
                            <td>Seaway Hawk</td>
                        </tr>
                        <tr>
                            <td>Western Geophysical</td>
                            <td class="text-center">1999-2003</td>
                            <td>Western Spirit, Western Inlet</td>
                        </tr>
                        <tr>
                            <td>Heerema Marine Contractors BV</td>
                            <td class="text-center">2000-2012</td>
                            <td>Hermod, Balder & Thialf 2007 + over 45 Tugs and Barges</td>
                        </tr>
                        <tr>
                            <td>Dockwise N.V.</td>
                            <td class="text-center">2007</td>
                            <td>Mighty Servant 2</td>
                        </tr>
                        <tr>
                            <td>Wijsmuller Marine Services</td>
                            <td class="text-center">2002-2004</td>
                            <td>Smitjis Tempest</td>
                        </tr>
                        <tr>
                            <td>Enterprise Philippe Lassarat S.A.</td>
                            <td class="text-center">2002-2006</td>
                            <td>Olgui One</td>
                        </tr>
                        <tr>
                            <td>Sealion Shipping Limited</td>
                            <td class="text-center">2005-2012</td>
                            <td>DSV Toisa Puma, Toisa Proteus, Toisa Pisces, Toisa Perseus</td>
                        </tr>
                        <tr>
                            <td>Halliburton</td>
                            <td class="text-center">2002-2004</td>
                            <td>Importation & Instalation of 5 Platforms in the bay of Campeche</td>
                        </tr>
                        <tr>
                            <td>P.M.I. Trading Limited</td>
                            <td class="text-center">1999-2004</td>
                            <td>M/T Trogir</td>
                        </tr>
                        <tr>
                            <td>Equiva Trading International</td>
                            <td class="text-center">1999-2004</td>
                            <td>Various Motor Tankers</td>
                        </tr>
                        <tr>
                            <td>Marine Transport Lines</td>
                            <td class="text-center">1999-2005</td>
                            <td>Various Motor Tankers</td>
                        </tr>
                        <tr>
                            <td>PDV Marina S.A.</td>
                            <td class="text-center">1999-2004</td>
                            <td>Various Motor Tankers</td>
                        </tr>
                        <tr>
                            <td>Chevron Shipping Company</td>
                            <td class="text-center">1999-2005</td>
                            <td>Various Motor Tankers</td>
                        </tr>
                        <tr>
                            <td>Diamond Drilling</td>
                            <td class="text-center">2000-2012</td>
                            <td>Ocean Ambassador, Ocean Worker, Ocean Yorktown, Ocean Whittington, Ocean Columbia, Ocean Voyager, Ocean Nugget, Ocean Summit</td>
                        </tr>
                        <tr>
                            <td>Dolphin Drilling</td>
                            <td class="text-center">2005-2007</td>
                            <td>Borgny Dolphin, Bulford Dolphin</td>
                        </tr>
                        <tr>
                            <td>Contrucciones Integrales del Carmen S.A. de C.V.</td>
                            <td class="text-center">2002-2007</td>
                            <td>Far Scotia, Far Swift, Norman Rover, Norman Flower. Island Pioneer</td>
                        </tr>
                        <tr>
                            <td>Todco Mexico Inc.</td>
                            <td class="text-center">2005-2012</td>
                            <td>Rig 003, THE 206, THE 205</td>
                        </tr>
                        <tr>
                            <td>Nabors Perforaciones de Mexico S. de RL. de CV.</td>
                            <td class="text-center">2000-2012</td>
                            <td>Cape Lookout, Cape Lobos, Cape Sable, Cape Hunter, Cape Bird, Cape Fear.</td>
                        </tr>
                        <tr>
                            <td>Western Geco</td>
                            <td class="text-center">1999-2009</td>
                            <td>Western Patriot, Geco Diamond, Western Regent</td>
                        </tr>
                        <tr>
                            <td>Corp. Mexica de Mantenimiento Integral S de RL de CV</td>
                            <td class="text-center">2002-2004</td>
                            <td>Importation & Installation of 1 Platform in the bay of Campeche</td>
                        </tr>
                        <tr>
                            <td>Songa Drilling AS</td>
                            <td class="text-center">2008-2009</td>
                            <td>Rig Songa Jupiter</td>
                        </tr>
                        <tr>
                            <td>C&C Technologies Geomar de Mexico</td>
                            <td class="text-center">2006-2007</td>
                            <td>M/V Moana Wave</td>
                        </tr>
                        <tr>
                            <td>Saipem Inc.</td>
                            <td class="text-center">2006-2008</td>
                            <td>Saipen 7000 + Over 15 Tugs and Barges</td>
                        </tr>
                        <tr>
                            <td>Fairfield Industries Inc.</td>
                            <td class="text-center">2007-2008</td>
                            <td>M/V Fairfield New Venture</td>
                        </tr>
                        <tr>
                            <td>Dredging International Mexico SA de CV</td>
                            <td class="text-center">2006-2008</td>
                            <td>M/V Tideway Rolling Stone</td>
                        </tr>
                        <tr>
                            <td>Pecten Trading</td>
                            <td class="text-center">2008-2009</td>
                            <td>Various Tankers</td>
                        </tr>
                        <tr>
                            <td>Seabed Geophysical AS</td>
                            <td class="text-center">2004-2005</td>
                            <td>Norman Tonjer – Kondor Explorer</td>
                        </tr>
                        <tr>
                            <td>EMGS AS</td>
                            <td class="text-center">2007-2012</td>
                            <td>Siem Mollie, Boa Thalassa, Atlantic Guardian</td>
                        </tr>
                        <tr>
                            <td>Ensco Drilling</td>
                            <td class="text-center">2008-2012</td>
                            <td>Ensco 81, 83, 89, 93 & 98</td>
                        </tr>
                        <tr>
                            <td>CGG Veritas</td>
                            <td class="text-center">2008-2009</td>
                            <td>Princess, Veritas Viking, Muning Explorer, Victory G</td>
                        </tr>
                        <tr>
                            <td>Goimar SA de CV</td>
                            <td class="text-center">2007-2011</td>
                            <td>Goimar 703, 704, Marinero & Bucanero</td>
                        </tr>
                        <tr>
                            <td>Fugro Chance de Mexico</td>
                            <td class="text-center">2009-2012</td>
                            <td>Universal Surveyor & Ocean Carrier</td>
                        </tr>
                        <tr>
                            <td>Siem Offshore AS</td>
                            <td class="text-center">2008-2010</td>
                            <td>Ocean Commander, Siem Dorado & Siem Mollie</td>
                        </tr>
                        <tr>
                            <td>Bergesen Offshore AS</td>
                            <td class="text-center">2008-2009</td>
                            <td>FPSO Yuum Kak Nap</td>
                        </tr>
                        <tr>
                            <td>Namese / Trico Marine</td>
                            <td class="text-center">2008-2011</td>
                            <td>Buffalo River, Carson River, James River, Powder River, Leight River, East River, Palma River</td>
                        </tr>
                        <tr>
                            <td>Capital Signal</td>
                            <td class="text-center">2006-2012</td>
                            <td>Rig Movements, Geophysical and Geotechnical Job</td>
                        </tr>
                        <tr>
                            <td>Bay Inelectra</td>
                            <td class="text-center">2007-2009</td>
                            <td>Huck up & Commissioning of 1 offshore platform</td>
                        </tr>
                        <tr>
                            <td>TMM</td>
                            <td class="text-center">2000-2012</td>
                            <td>Offshore fleet of Crew Boats, AHTS, and OSV</td>
                        </tr>
                        <tr>
                            <td>Crosco</td>
                            <td class="text-center">2008-2010</td>
                            <td>SSDR – Zagreb 1</td>
                        </tr>
                        <tr>
                            <td>TAG Offshore Limited</td>
                            <td class="text-center">2009-2010</td>
                            <td>TAG 5</td>
                        </tr>
                        <tr>
                            <td>REM Offshore AS</td>
                            <td class="text-center">2010-2011</td>
                            <td>REM Forza</td>
                        </tr>
                        <tr>
                            <td>Ranger Offshore LLC</td>
                            <td class="text-center">2010-2012</td>
                            <td>DSV Hammerhead</td>
                        </tr>
                        <tr>
                            <td>Solstad Shipping AS</td>
                            <td class="text-center">2010-2015</td>
                            <td>Normand Fortress</td>
                        </tr>
                        <tr>
                            <td>Mermaid Marine Asia PTE LTD</td>
                            <td class="text-center">2010-2012</td>
                            <td>M/V Mermaid Vigilance</td>
                        </tr>
                        <tr>
                            <td>Solstad Offshore Asia Pacific Ltd.</td>
                            <td class="text-center">2010-2015</td>
                            <td>M/V Nor Valiant</td>
                        </tr>
                        <tr>
                            <td>CGG Veritas AS</td>
                            <td class="text-center">2009-2013</td>
                            <td>M/V Alize & Venture G, Seismic Contract</td>
                        </tr>
                        <tr>
                            <td>CGG Veritas Services Mexico S.A. de C.V.</td>
                            <td class="text-center">2010-2012</td>
                            <td>Seismic WAZ project : M/V Oceanic Sirius, Oceanic Vega, Neptune Naiad,Venturer, Fairfield Challenger, Bergen Surveyor, Hos Thunderfoot, Kurt David, Palizada</td>
                        </tr>
                        <tr>
                            <td>Rederij Groen B.V.</td>
                            <td class="text-center">2009-2012</td>
                            <td>Victory G & Venture G<td>
                        </tr>
                        <tr>
                            <td>Boa SBL, AS</td>
                            <td class="text-center">2010-2012</td>
                            <td> M/V Boa Thalassa<td>
                        </tr>
                        <tr>
                            <td>Geokinetics</td>
                            <td class="text-center">2009-2012</td>
                            <td>M/V Geotiger 2,3, & 4, Snapper, Trinity I & II, Mermaid Vigilance, Hammerhead, East River<td>
                        </tr>
                        <tr>
                            <td>Mammoet Salvage</td>
                            <td class="text-center">2011</td>
                            <td> M/V Rem Forza, Ulysses, Resolve Pioneer, Salvage Job Jupiter 1<td>
                        </tr>
                        <tr>
                            <td>Subsea 7</td>
                            <td class="text-center">2011-2012</td>
                            <td>Various Offshore Projects<td>
                        </tr>
                        <tr>
                            <td>Seadrill</td>
                            <td class="text-center">2011-2012</td>
                            <td>SSDP – West Pegasus<td>
                        </tr>
                        <tr>
                            <td>Seadrill</td>
                            <td class="text-center">2013-2015</td>
                            <td>West Titania, West Courageous, West Defender, West Oberon, West Intrepid<td>
                        </tr>
                        <tr>
                            <td>SapuraAcergy</td>
                            <td class="text-center">2012</td>
                            <td>Sapura 3000. Platform Installation in Mexico<td>
                        </tr>
                        <tr>
                            <td>Heerema Marine Contractors Americas BV</td>
                            <td class="text-center">2013-2015</td>
                            <td>Balder – Mexico Ayatsil Project<td>
                        </tr>
                        <tr>
                            <td>Oro Negro</td>
                            <td class="text-center">2012-2014</td>
                            <td>Decus, Fortius, Laurus, Primus<td>
                        </tr>
                        <tr>
                            <td>Dulam International Limited</td>
                            <td class="text-center">2013</td>
                            <td>DSV Oceanic Installer.<td>
                        </tr>
                        <tr>
                            <td>Constructora Subaquatica Diavaz</td>
                            <td class="text-center">2014</td>
                            <td>Seacor Diamond. SPM Maintenance in Salina Cruz.<td>
                        </tr>
                        <tr>
                            <td>Seaway Heavy Lift</td>
                            <td class="text-center">2014</td>
                            <td>Olegstrashnov<td>
                        </tr>
                        <tr>
                            <td>Sealion Shipping Limited LTD</td>
                            <td class="text-center">2012-2015</td>
                            <td>Toisa Pisces, Toisa Proteus, Toisa Perseus<td>
                        </tr>
                        <tr>
                            <td>Heerema Marine Contractors</td>
                            <td class="text-center">2015</td>
                            <td>Marmac 400, H122<td>
                        </tr>
                        <tr>
                            <td>GXT</td>
                            <td class="text-center">2015</td>
                            <td>Calvin, Discovery 1, Discovery 2, Orient Pearl, Alida Matascha / Seismic Work.<td>
                        </tr>
                        <tr>
                            <td>SapuraKencana</td>
                            <td class="text-center">2015</td>
                            <td>HLV SK 3500 / Platform Installations and Pipelines.<td>
                        </tr>
                        <tr>
                            <td>SapuraKencana</td>
                            <td class="text-center">2015</td>
                            <td>Cindy Tide<td>
                        </tr>
                        <tr>
                            <td>Western Geophysical</td>
                            <td class="text-center">2015</td>
                            <td>Odyssey, Columbus, Cook, Tasman, Geco Diamond, Patriot, Eagle, Neptune, Warrior, Mainport Pine, Mainport Kells, Maria G, Aquarius G, Melinda B Adams / Seismic Work.<td>
                        </tr>
                        <tr>
                            <td>Dolphin Geophysical</td>
                            <td class="text-center">2015</td>
                            <td>Artermis Arctiv / Seismic Work.<td>
                        </tr>
                        <tr>
                            <td>Drillmec / Baggio</td>
                            <td class="text-center">2015</td>
                            <td>Sabalo<td>
                        </tr>
                        <tr>
                            <td>Nabors Drilling</td>
                            <td class="text-center">2015</td>
                            <td>Sabalo<td>
                        </tr>
                        <tr>
                            <td>Baggio International Shipping & Forwarding</td>
                            <td class="text-center">2015</td>
                            <td>Sabalo<td>
                        </tr>
                        <tr>
                            <td>Sapurakencana Mexicana S.A.P.I de C.V.</td>
                            <td class="text-center">2015</td>
                            <td>Cindy Tide<td>
                        </tr>
                        <tr>
                            <td>Dolphin Geophysical DE Mexico S.A. de C.V.</td>
                            <td class="text-center">2015</td>
                            <td>Artemis Arctic<td>
                        </tr>
                        <tr>
                            <td>Dowell Schlumberger DE Mexico S.A. de C.V.</td>
                            <td class="text-center">2015-2017</td>
                            <td>Melinda B Adams, Aquarius G, Astra G, Maria G, Mainport Kells, Mainport
                                Pine, Mainport Cedar, Ocean Odyssey, Wg Cook, Wg Columbus, Western Patriot, Western Neptune, Geco Eagle, Geco Diamond, Amazon Warrior,
                                Wg Vespucci, Geco Emerald, Geco Scorpio, Geco Topaz, Wg Tasman, Wg Magellan<td>
                        </tr>
                        <tr>
                            <td>GX Geoscience Corporation S. de R.l. de C.E.</td>
                            <td class="text-center">2015</td>
                            <td>Discoverer 2, Dong Fang Ming Zhu, Calvin, Alida Natascha<td>
                        </tr>
                        <tr>
                            <td>Heerema Shipping 3, B.V.</td>
                            <td class="text-center">2014-2015</td>
                            <td>Marmac 400, H122, Crosby Odyssey, Crosby Moon, Oceanus, 400 L, MWB-40<td>
                        </tr>
                        <tr>
                            <td>Sapurakencana Mexicana S.A.P.I de C.V.</td>
                            <td class="text-center">2015-2016</td>
                            <td>Sapurakencana 3500<td>
                        </tr>
                        <tr>
                            <td>Solstad Shipping As.</td>
                            <td class="text-center">2017-2020</td>
                            <td>Normand Oceanic<td>
                        </tr>
                        <tr>
                            <td>Fieldwood Energy</td>
                            <td class="text-center">2017-2018</td>
                            <td>Nn Chort, Nn Algeiba,Nn Zosma, Isla Monserrat, Isla Janitzio. Isla Azteca, Hebert Tide, Pcb - Isla Arboleda, Eco Iii<td>
                        </tr>
                        <tr>
                            <td>Fugro Mexico S.A. de C.V.</td>
                            <td class="text-center">2015-2018</td>
                            <td>Chartres, Hos Deepwater<td>
                        </tr>
                        <tr>
                            <td>FarStad Shipping AS</td>
                            <td class="text-center">2018-2020</td>
                            <td>Sentinel<td>
                        </tr>
                        <tr>
                            <td>Sapurakancana Mexicana S.A.P.I. de C.V.</td>
                            <td class="text-center">2018</td>
                            <td>SK 3500<td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </section>


@endsection

@section('footer')


    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.js"></script>

    <script>
        $(document).ready(function(){
            $('#myTable').DataTable({
                paging: false,
                "order": [[ 1, "asc" ]]
            });
        });
    </script>
@endsection