@extends('layout')

@section('content')
    <style>
        .text-center span {color:#2f41c9; font-weight:bold; }
    </style>

    <section class="inner-banner about">
        <div class="thm-container">
            <h2 class="text-center" style="text-shadow: 1px 2px 4px #000">Our Companies</h2>
            <br>
        </div>
    </section>


    <section class="featured-services">
        <div class="thm-container">
            <div class="row" style="margin-bottom:70px;" >
                <div class="col-md-12">
                    <img src="/images/locations/worldlocations2.png" class="center-block" width="100%" style="margin-top:50px;"/>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="row" style="margin-top:30px;">
                <div class="col-md-3">
                    <img src="images/flags2/mexican.png" alt=""  class="center-block">
                    <h3 class="text-center"><span>Canega S.A. de C.V.</span></h3>
                    <h3 class="text-center"><span>(Est. 1923)</span></h3>
                </div>
                <div class="col-md-9">
                    <h3 style="text-align: justify">With Headquarters in Cd. del Carmen, Campeche, Mexico, this member of Canega Group provides a wide-range of Port Agency Services in the ports of Cd. del Carmen, Dos Bocas, Coatzacoalcos, Tuxpan and Tampico, Mexico. It is also the Headquarters for all Offshore Operations in Mexico and related services for our domestic and international clients working and servicing the Pemex Oil Fields in the Bay of Campeche.
                        <br>
                        <br>
                        When Pemex discovered oil in the Bay of Campeche in 1979, they set up base in Cd. del Carmen. Canega already being the exclusive agent of Pemex on the island for their fuel deliveries and the only company licensed to operate as a shipping agent and customs broker, Pemex called upon Canega to provide, coordinate and conduct all logistics and operations needed for the development of the offshore oil fields and export facilities in the Bay of Campeche including the first crude oil export of Mexico.
                        <br>
                        <br>
                        A year later once again Pemex called upon Canega to arrange and coordinate all logistics and operations in response to the explosion, fire and oil spill of the drilling rig “Ixtoc”. A year later, Canega once again was entrusted to attend and provide all the required port agency services for all crude oil and lightering operations in the Bay of Campeche.
                        <br>
                        <br>
                        We are very proud to have been and continue to be an intricate key factor in the development of the offshore oil industry in Mexico. We were the only ones here at the beginning…  We are <b>“The Pioneers of the Offshore Industry in the Bay of Campeche since 1979″</b>, a very important fact that no other company in Mexico can ever claim.</h3>
                </div>
            </div>
            <hr>
            <div class="row" style="margin-top:30px;">
                <div class="col-md-3">
                    <img src="images/flags2/american.png" alt="" width="" class="center-block">
                    <h3 class="text-center"><span>Canega Shipping Services</span></h3>
                    <h3 class="text-center"><span>(Est. 1983)</span></h3>
                </div>
                <div class="col-md-9">
                    <h3 style="text-align: justify">
                        Established in 1983, this company serves as the Headquarters of Canega Group in the United States. Besides being responsible for all marketing for Canega Group, it is the Hub for all Agency Services provided in the U.S. Gulf, East/West Coast of Mexico, Caribbean, Central and South America. The office located in Wesley Chapel, Florida operates on a 24 hrs. basis, 7 days a week in support of all Canega Group operations and services in over 30 countries. Please refer to Areas of Service for a complete overview of the regions serviced.
                        <br>
                        <br>
                        In 1995, Canega Shipping Services was awarded its ISO 9002 Certification by American Bureau of Shipping (ABS). In doing so, once again Canega became the leader in the Western Hemisphere by becoming The First Shipping Agency and Bunker Broker to be ISO 9002 Certified in the United States, Mexico, Caribbean, Central and South America. Since no shipping agency had been certified before in the services mentioned,  ABS used Canega’s Quality Assurance Program as the guideline for the certification of future shipping agencies. Today, Canega continues to utilize and improve its well proven Quality Assurance Program.
                    </h3>
                </div>
            </div>
            <hr>
            <div class="row" style="margin-top:30px;">
                <div class="col-md-3">
                    <img src="images/flags2/nv.png" alt="" width="" class="center-block">
                    <h3 class="text-center"><span>Canega Shipping Services N.V.</span></h3>
                    <h3 class="text-center"><span>(Est. 1995)</span></h3>
                </div>
                <div class="col-md-9">
                    <h3 style="text-align: justify">
                        In 1994, when Saudi Petroleum International and Vela International Marine Ltd., moved its oil shipments to the Western Hemisphere from the Island of Aruba to the Island of St. Eustatius, they searched around the Caribbean for a company that had the experience, knowledge, reputation, financial stability, integrity, quality system and infrastructure to provide all the Agency Services needed by Vela International Marine Ltd., large (VLCC) and very large (ULCC) crude carriers. There was only one company that met all their criteria’s… CANEGA
                        <br>
                        <br>
                        At their special request, this company was established and its office opened in the Island of St. Eustatius to service the exclusive contract awarded to Canega which lasted 15 years. Today, this company continues to provide second to none agency services and remains the only fully independent Port Agency with a Certified Quality Assurance program in St. Eustatius.
                        <br>
                        <br>
                        Saudi Petroleum International and Vela International Marine Ltd, trusted Canega with their business in the Caribbean based in part on our reputation, experience and infrastructure. In appreciation for the trust and confidence they placed in our company, we are proud to show here the first ever oil tanker that called the terminal of St. Eustatius on January, 1995.
                    </h3>
                </div>
            </div>
            <hr>
            <div class="row" style="margin-top:30px;">
                <div class="col-md-3">
                    <img src="images/flags2/mexican.png" alt="" width="" class="center-block">
                    <h3 class="text-center"><span>Canega Internacional <br>S.A. de C.V.</span></h3>
                    <h3 class="text-center"><span>(Est. 2005)</span></h3>
                </div>
                <div class="col-md-9">
                    <h3 style="text-align: justify">
                        This member of Canega Group is the Ship Owner, Time Charterer and Operator of Mexican and Foreign flag offshore vessels that work in the Bay of Campeche supporting our domestic and international clients working for Pemex. Furthermore,  as a registered Mexican Ship Owner, this company can obtain and secure the Navigation Permits required by Mexican Law on all foreign flag vessels working in Mexico.
                    </h3>
                </div>
            </div>
            <hr>

            <div class="row" style="margin-top:30px;">
                <div class="col-md-3">
                    <img src="images/flags2/american.png" alt="" width="" class="center-block">
                    <h3 class="text-center"><span>Canega Offshore Services</span></h3>
                    <h3 class="text-center"><span>(Est. 2010)</span></h3>
                </div>
                <div class="col-md-9">
                    <h3 style="text-align: justify">
                        This company is the Chartering, Brokering and Sales arm of Canega Group. All foreign flag offshore support vessels operating in Mexico, U.S. Gulf, Latin America, West Africa, the Middle East, Mediterranean and other parts of the world, are chartered and operated by this company from its head office located in the  United States. Canega has done 95% of all Geophysical and Geotechnical work in the Gulf of Mexico.
                    </h3>
                </div>
            </div>
            <hr>
            <div class="row" style="margin-top:30px;">
                <div class="col-md-3">
                    <img src="images/flags2/uae.png" alt="" width="" class="center-block">
                    <h3 class="text-center"><span>Canega Trading FZE</span></h3>
                    <h3 class="text-center"><span>(Est. 2013)</span></h3>
                </div>
                <div class="col-md-9">
                    <h3 style="text-align: justify">
                        Established in the UAE to expand Canega’s operations into the Middle East, Mediterranean and West Africa. We are presently working on several possible projects which we expect to be awarded soon.
                    </h3>
                </div>
            </div>
            <hr>
            <div class="row" style="margin-top:30px;margin-bottom:70px;">
                <div class="col-md-3">
                    <img src="images/flags2/singapore.png" alt="" width="" class="center-block">
                    <h3 class="text-center"><span>Canega International Pte. Ltd.</span></h3>
                    <h3 class="text-center"><span>(Est. 2015)</span></h3>
                </div>
                <div class="col-md-9">
                    <h3 style="text-align: justify">
                        Established in Singapore as the foreign flag ship owning company of Canega Group. All new vessel acquisitions for Canega Group International operations in West Africa, Mediterranean and the Middle East will be done through this company.
                    </h3>
                </div>
            </div>
        </div>
    </section>

@endsection