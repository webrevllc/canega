@extends('layout')

@section('content')


    <section class="inner-banner about">
        <div class="thm-container">
            <h2 class="text-center" style="text-shadow: 1px 2px 4px #000">Company History</h2>
            <br>
            {{--<ul class="breadcumb">--}}
                {{--<li><a href="/"><i class="fa fa-home"></i> Home</a></li>--}}
                {{--<li><span>Company History</span></li>--}}
            {{--</ul>--}}
        </div>
    </section>
    {{--<h1 class="text-center">Company History</h1>--}}

    <section class="faq-page career-page" style="padding-top: 40px;">
        <div class="thm-container">
            <div class="row">
                <div class="col-md-12">
                    {{--<h2>Our History</h2>--}}
                    <div class="col-xs-12 col-sm-6">
                        <ul class="history">
                            <li>
                                <b>1923</b> - Casa Negroe Garcia (Canega) was founded in Mexico.
                            </li>
                            <div class="highlighted">
                                <li>
                                    <b>1937</b> – Appointed Exclusive Pemex Agents in Cd. Del Carmen, Mexico.
                                </li>
                                <li><b>1979</b> - Exclusive appointment by Pemex to provide all services
                                    needed for the development of the Offshore Oil Fields and
                                    Export Facilities in the Bay of Campeche.
                                </li>
                                <li>
                                    <b>1980</b> - Exclusive appointment by Pemex to arrange and coordinate all
                                    logistics and operations in Cd. del Carmen in response to the
                                    explosion, fire and 9 month oil spill of drilling rig “Ixtoc”.
                                </li>
                                <li>
                                    <b>1981</b> - Appointed exclusive Agents of Pemex to attend all crude oil exports
                                    and lightering operations from the Bay of Campeche, Mexico.
                                </li>
                            </div>

                            <li>
                                <b>1983</b> - Established office Florida to carry out all marketing, logistics,
                                operations and billing with worldwide clients.
                            </li>
                            <li>
                                <b>1983</b> - Began expansion in the U.S. Gulf, Caribbean, Central & South
                                America to facilitate service the operations of our rapidly
                                growing worldwide customer base.
                            </li>
                            <li>
                                <b>1985</b> - Opened offices in Dos Bocas & Coatzacoalcos, Mexico.
                            </li>
                            <li>
                                <b>1987</b> - Set up Bunker Broker Department and became First and Only
                                company to start marketing and selling bunkers in Mexico.
                            </li>
                            <li>
                                <b>1988</b> - Continued expanding operations in Latin America.
                            </li>
                            <div class="highlighted">
                                <li>
                                    <b>1990</b> - Moved and established World Headquarters in Houston, Texas.
                                </li>
                            </div>
                            <li>
                                <b>1992</b> - Opened offices in Tampico & Altamira, Mexico.
                            </li>
                            <li>
                                <b>1992</b>- Opened first office in Maracaibo, Venezuela.
                            </li>
                            <li>
                                <b>1994</b> - Set up Ship Brokering and Chartering Department
                            </li>
                            <div class="highlighted">
                                <li>
                                    <b>1994</b> - Appointed exclusive Agents of Marathon Oil Company &
                                    opened New Orleans office to attend all their vessels in
                                    Garyville and LOOP, La.
                                </li>
                                <li>
                                    <b>1995</b> - Appointed exclusive Agents of Vela and Saudi Petroleum in St. Eustatius,
                                    Netherlands Antilles
                                </li>
                                </div>
                            <li>
                                <b>1995</b> - Expanded Bunker Brokering Services Worldwide.
                            </li>
                            <li>
                                <b>1995</b> - Opened office in Puerto La Cruz, Venezuela.
                            </li>
                            <div class="highlighted">
                                <li>
                                    <b>1995</b> - Became First Shipping Agency & Bunker Broker to be ISO
                                    9002 Certified in the U.S. and throughout Latin America.
                                </li>
                                <li>
                                    <b>1996</b> - Appointed by PDV Marina (Petroleos de Venezuela Marine
                                    Dept.) as their exclusive Port Agent in over 20 countries and
                                    primary Bunker Broker.
                                </li>
                                <li>
                                    <b>1996</b> - Selected by Chevron Shipping Company as exclusive Port
                                    Agent and signed Strategic Alliance for all of Latin America.
                                </li>
                            </div>
                            <li>
                                <b>1996</b> - Internet Web Site Released.
                            </li>
                            <div class="highlighted">
                                <li>
                                    <b>1997</b> - Opened office in Amuay Bay, Venezuela to attend all vessels of
                                    Coastal Petroleum.
                                </li>
                            </div>

                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <ul class="history">

                            <li>
                                <b>1997</b> - Appointed exclusive Port Agents by Norwegian Gas Carriers in
                                the U.S. Gulf.
                            </li>
                            <li>
                                <b>1997</b> - Selected by Bunker News as member to their Exclusive
                                Bunker Broker Pricing Panel.
                            </li>
                            <li>
                                <b>1997</b> - First Edition of Canega’s On-line Port Information Released.
                            </li>
                            <li>
                                <b>2002</b> - Moved Headquarters back to Florida.
                            </li>
                            <li>
                                <b>2003</b> - Enter into Strategic Alliance with Gulf Agency Company to
                                attend all their vessels calling St. Eustatius, Netherlands
                                Antilles.
                            </li>
                            <div class="highlighted">
                                <li>
                                    <b>2007</b> – Appointed Exclusive Agents of Trafigura in
                                    St. Eustatius, Netherlands Antilles.
                                </li>
                                <li>
                                    <b>2014</b> – Signed exclusive Technical Partnership with Petro-Pride
                                    Subsea Limited in Nigeria to provide them with all types
                                    of vessels needed to support their service contracts with
                                    Exxon-Mobil.
                                </li>
                                <li>
                                    <b>2014</b> – Appointed exclusive representative for Mexico and Latin
                                    America of Savante Offshore Services Ltd. Companies will
                                    jointly market inspection services to Offshore Installations
                                    using lasers technology.
                                </li>
                                <li>
                                    <b>2014</b> – Article Published in Mexico Oil & Gas Book about the history
                                    of Canega and its contribution to the development of the
                                    Offshore Industry in the Bay of Campeche, Mexico.
                                </li>
                                <li>
                                    <b>2014</b> – Appointed exclusive representative of Savante Offshore
                                    Services Ltd. Companies will offer and provide inspection
                                    services to the Offshore Oil & Gas Industry utilizing laser
                                    technology.
                                </li>
                                <li>
                                    <b>2015</b> – Acquisition of our Flagship “Papito” an Offshore Support
                                    Vessel. Additional Vessels will be added to the fleet in 2016.
                                </li>
                                <li>
                                    <b>2016</b> – Signed exclusive Technical Partnership with CNS Marine
                                    Nigeria Ltd., to provide them with vessels needed to support
                                    their service contracts with several Major Oil Companies.
                                </li>
                                <li>
                                    <b>2016</b> - Signed exclusive MOU with CNS International SRL, to jointly
                                    market and provide for the first time IMCA Certified Diving
                                    Services in Mexico as part of the Energy Reform as well as
                                    Nigeria and other worldwide areas.
                                </li>
                                <li>
                                    <b>2016</b> - Signed exclusive MOU with Houston based ROVOP to jointly
                                    market and provide ROV Services in Mexico as part of the
                                    Energy Reform.
                                </li>
                                <li>
                                    <b>2016</b> - Signed exclusive MOU with Venezuelan based IVN to provide
                                    them with vessels, IMCA Certified Diving Services & ROV
                                    Services needed to support their service contracts with
                                    several Major Oil Companies
                                </li>
                                <li>
                                    <b>2016</b> - Canega becomes the First Mexican Shipowner to be in full
                                    compliance with OVID as required by OCIMF.
                                </li>
                                <li>
                                    <b>2016</b> - Signed exclusive MOU between CNS Marine Nigeria and Grupo Protexa for pipe laying and platform installation services in West Africa.
                                </li>
                                <li>
                                    <b>2018</b> - Signed Master Service Agreement with Oceaneering International for the provision of port, marine and vessel services in Mexico.
                                </li>
                                <li>
                                    <b>2018</b> - Signed exclusive MOU between Petro-Pride Subsea Limited and Grupo Protexa for pipe laying and platform installation services in Nigeria.
                                </li>

                            </div>
                        </ul>
                    </div>

                    {{--<iframe src='https://cdn.knightlab.com/libs/timeline3/latest/embed/index.html?source=1rS5GzGdbolEpDX5BjCvJn1HYhQsOcc7L-QlnTNKFYg8&font=Default&lang=en&initial_zoom=2&height=650' width='100%' height='650' frameborder='0'></iframe>--}}
                </div>
            </div>
        </div>
    </section>


@endsection