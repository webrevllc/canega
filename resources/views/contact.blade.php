@extends('layout')
@section('content')

<section class="inner-banner">
    <div class="thm-container">
        <h2 class="text-center">Contact Us</h2>
    </div>
</section>

<section class="sec-padding contact-page-content">
    <div class="thm-container">
        <div class="sec-title">
            <h2><span>Get in touch</span></h2>
        </div>
        <div class="row">
            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    <h4>{{ Session::get('success') }}</h4>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">
                    <h4>{{ Session::get('error') }}</h4>
                </div>
            @endif
            <div class="col-md-7 col-sm-6 col-xs-12 pull-left">
                <form action="/contactpost" method="post" class="contact-form contact-page">
                    {{csrf_field()}}
                    <p><input type="text" placeholder="Name" name="name"></p>
                    <p><input type="text" placeholder="Email" name="email"></p>
                    <p><input type="text" placeholder="Subject" name="subject"></p>
                    <p><textarea name="message" placeholder="Message"></textarea></p>
                    <button type="submit" class="thm-btn">Submit Now <i class="fa fa-arrow-right"></i></button>
                </form>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 pull-right">
                <div class="contact-info">
                    <ul>
                        <li>
                            <div class="icon-box">
                                <i class="icon icon-Pointer"></i>
                            </div>
                            <div class="content">
                                <p>Canega Offshore Services<br> 27212 Breakers Drive <br>Wesley Chapel, FL 33544</p>
                            </div>
                        </li>
                        <li>
                            <div class="icon-box">
                                <i class="icon icon-Plaine"></i>
                            </div>
                            <div class="content">
                                <p>Chartering Dept: chartering@canega.com <br>Port Agency Dept: canops@canega.com</p>
                            </div>
                        </li>
                        <li>
                            <div class="icon-box">
                                <i class="icon icon-Phone2"></i>
                            </div>
                            <div class="content">
                                <p>(813) 907-3376</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection