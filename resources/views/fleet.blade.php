@extends('layout')

@section('content')


    <section class="inner-banner service">
        <div class="thm-container">
            <h2 class="text-center">Our Fleet</h2>
        </div>
    </section>

    
    <section class="fleet-gallery blog-page single-post-page" style="margin-top: 50px;">
        <div class="container">
            <div class="sec-title">
                <br>
                <h2><span>About Our Fleet</span></h2>
                <br>
                <h4 class="text-justify">
                    In 2015 Canaga Group started its vessel acquisition program of new generation Multi-Purpose Service Vessels, Platform Supply Vessels, and Fast Intervention Supply Vessels to meet the requirements of our International clients.
                    We support every phase of offshore exploration, development and production and transporting the supplies and personnel needed to sustain drilling, research, workover and production activities. Canega also provides the direct assistance needed for specialized services such as air and saturation diving, ROV, pipe laying, cable laying, seismic work and platform installation.
                    <br>
                    <br>
                    Our Technical and Crewing Manager, Thome Offshore Management PTE LTD has extensive worldwide operating experience and offers versatile solutions in the management of Offshore Support Vessels (OSVs) which can be tailored to fit the needs and demands of its clients and operational area.
                    <br>
                    <br>
                    Canega maintains offices at strategic worldwide locations to ensure the readiness of technical support that is thoroughly familiar with the logistics, politics and cultural sensitivities of every region our vessels operate. From short-term special projects to long-term exploration, development and production. Canega customers receive efficient, personal and knowledgeable service of the highest quality.

                </h4>
            </div>
                <div class="sec-title">
                    <br>
                    <h2><span>MPSV (Multi-Purpose Service Vessel)</span></h2>
                </div>
                <table class="table-bordered table table-striped" width="100%">
                    <thead class="thead-inverse text-center">
                    <th>Name</th>
                    <th>Built</th>
                    <th>Class</th>
                    <th>Crane</th>
                    <th>4 PT Mooring</th>
                    <th>Beds</th>
                    <th>Fire Fighting</th>
                    <th>PDF</th>
                    <th>Image</th>
                    </thead>
                    <tbody>
                    @foreach($mpsv as $v)
                        <tr>
                            <td class="col-md-2 text-center">{{$v->name}}</td>
                            <td class="col-md-1 text-center">{{$v->built}}</td>
                            <td class="col-md-2 text-center">{{$v->class}}</td>
                            <td class="col-md-1 text-center">{{$v->crane}}</td>
                            <td class="col-md-2 text-center">{{$v->mooring}}</td>
                            <td class="col-md-2 text-center">{{$v->beds}}</td>
                            <td class="col-md-2 text-center">{{$v->firefighting}}</td>
                            <td class="col-md-1 text-center"><a href="{{$v->pdf_url}}"  target="_blank" ><img class="center-block" src="/images/pdf-icon.png" width="100%"/></a></td>
                            <td class="col-md-1 text-center">
                                @if($v->image_url != '')
                                    <a href="{{$v->image_url}}" data-lity><img src="{{$v->image_url}}" width="100%" class="center-block"/></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            <br>
            <br>
            <br>

            <div class="sec-title">
                <h2><span>FISV (Fast Intervention Supply Vessels)</span></h2>
                {{--<h4>Few companies can boast the wide range of crewboats Canega operates around the world. With the capacity to safely transport as many as 150 passengers and moderate quantities of cargo at high speed and  in comfort, Canega's crewboat fleet has the versatility to meet a variety of production needs in the most demanding of conditions.</h4>--}}
            </div>
            <table class="table-bordered table table-striped" width="100%">
                <thead class="thead-inverse text-center">
                <th>Name</th>
                <th>Built</th>
                <th>Class</th>
                <th>LOA</th>
                <th>Pax</th>
                <th>Speed</th>
                <th>Fire Fighting</th>
                <th>PDF</th>
                <th>Image</th>
                </thead>
                <tbody>
                @foreach($fisv as $v)
                    <tr>
                        <td class="col-md-2 text-center">{{$v->name}}</td>
                        <td class="col-md-1 text-center">{{$v->built}}</td>
                        <td class="col-md-2 text-center">{{$v->class}}</td>
                        <td class="col-md-2 text-center">{{$v->loa}}</td>
                        <td class="col-md-1 text-center">{{$v->pax}}</td>
                        <td class="col-md-2 text-center">{{$v->speed}}</td>
                        <td class="col-md-2 text-center">{{$v->firefighting}}</td>
                        <td class="col-md-1 text-center"><a href="{{$v->pdf_url}}"  target="_blank" ><img class="center-block" src="/images/pdf-icon.png" width="100%"/></a></td>
                        <td class="col-md-1 text-center">
                            @if($v->image_url != '')
                                <a href="{{$v->image_url}}" data-lity><img src="{{$v->image_url}}" width="100%" class="center-block"/></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection