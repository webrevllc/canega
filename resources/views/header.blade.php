<section id="top-bar">
    <div class="thm-container clearfix">
        <div class="phone pull-left">
            <p>Call Us Now: <span>(813) 907-3376</span></p>
        </div>
        <div class="top-contact-info pull-right">
            <ul>
                <li><i class="fa fa-map-marker"></i> 27212 Breakers Drive, Wesley Chapel FL 33544</li>
                <li><i class="fa fa-envelope"></i>  Chartering Dept: chartering@canega.com</li>
            </ul>
        </div>
    </div>
</section>

<header id="header3" class="">
    <div class="thm-container clearfix">
        <div class="logo pull-left">
            <a href="/">
                <img src="/images/logos/Canega-Group-wSlogan.png" alt="">
                {{--<img src="/images/logos/headerlogo2.png" alt="">--}}
            </a>
        </div>
        <div class="nav-holder pull-right">
            <div class="nav-header">
                <button><i class="fa fa-bars"></i></button>
            </div>
            <div class="nav-footer">
                <ul class="nav">
                    <li >
                        <a href="/">Home</a>
                    </li>
                    <li class="has-submenu">
                        <a href="#">about us</a>
                        <ul class="submenu">
                            <li><a href="/about">About Canega</a></li>
                            <li><a href="/company-history">Company History</a></li>
                            <li><a href="/clients">Our Clients & Chartering Record</a></li>
                            <li><a href="/companies">Our Companies</a></li>
                            {{--<li><a href="/pols">Our Policies</a></li>--}}
                            <li><a href="/certifications">Certifications &amp; Memberships</a></li>
                            <li><a href="/testimonials">Testimonials</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="/services">Services</a>
                    </li>
                    {{--<li >--}}
                        {{--<a href="/fleet">Our Fleet</a>--}}
                    {{--</li>--}}
                    <li >
                        <a href="/locations">Service Areas</a>
                    </li>
                    <li><a href="/contact">contact us</a></li>
                </ul>
            </div>
        </div>
    </div>

</header>
<div class="container">
    <div class="row certification-logos">
        <img src="/images/certifications/New/1.png" />
        <img src="/images/certifications/New/2.png" />
        <img src="/images/certifications/New/3.png" />
        <img src="/images/certifications/New/4.png" />
    </div>
</div>
