@extends('layout')

@section('content')

    <div class="container">
        <marquee class="marquee" behavior=scroll direction="left" scrollamount="5">
            The Sea Is Our World &middot;
            Celebrating {{Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(1923))}} years of serving the maritime, oil and gas industry
        </marquee>
        @include('slider')
    </div>

    <section class="welcome-text" style="background: #fff;">
        <div class="thm-container">
            <div class="title-box text-right">
                <h3 style="color:#183650">We Are <br>Pioneers</h3>
            </div>
            <div class="text-box">
                <h2 style="color:#183650;text-align: justify">The Pioneer and Leader of the Offshore Oil & Gas
                    Industry in the Bay of Campeche, Mexico since 1979
                    and the ONLY Mexican Shipping Company with
                    Worldwide Operations since 1983.</h2>
            </div>
        </div>
    </section>


    <section class="call-to-action">
        <div class="thm-container">
            <div class="row">
                <div class="col-md-7 hidden-xs hidden-sm">
                    <div class="right-full-image">
                        <img src="images/vessels/photo-17.JPG" alt="Awesome Image"/>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="call-to-action-text">
                        <h3>Prided in Quality</h3>
                        <p>Quality Service is the means of achieving and exceeding the required standards to satisfy the needs and to ensure strict adherence to our Clients requirements for a safe, efficient and cost-effective operation.</p>
                        <a href="/services" class="thm-btn">View our services <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="faq-section sec-padding home-two">
        <div class="thm-container">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <div class="sec-title">
                        <h2><span>our core values</span></h2>
                    </div>
                    <div class="accrodion-grp" data-grp-name="faq-accrodion">
                        <div class="accrodion active">
                            <div class="accrodion-title">
                                <h4>Our Mission</h4>
                            </div>
                            <div class="accrodion-content">
                                <div class="img-caption">
                                    <div class="img-box">
                                        <img src="/images/vessels/vision.jpg" alt="Awesome Image"/>
                                    </div>
                                    <div class="content-box">
                                        <h4>Quality Service is #1</h4>
                                        <p>Provide our customer with quality, professional and dependable service and deliver 100% customer satisfaction. Superior service is the means by which we achieve customer satisfaction. Underlying this mission is our Total Commitment to Quality Control, Value and Service Reliability. This commitment is carried out through Canega’s Quality Assurance Program. We want to be your ONE-STOP Shipping Company.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accrodion ">
                            <div class="accrodion-title">
                                <h4>Our Vision</h4>
                            </div>
                            <div class="accrodion-content">
                                <div class="img-caption">
                                    <div class="img-box">
                                        <img src="images/vessels/mission.JPG" alt="Awesome Image"/>
                                    </div>
                                    <div class="content-box">
                                        <p>Provide the maritime, oil & gas industries with a wide range of services, while providing outstanding quality service at every level of our organization. Recognize that quality is more than just meeting our client’s basic needs and requirements; the continuous education and training of our experienced personnel with Quality Service in mind and work as a TEAM with our clients to develop strategic alliances and partnerships.</p>
                                        <ul>
                                            • Provide Oil & Maritime Industries with a wide-range of services<br>
                                            • Provide Quality Service at every level of our Organization<br>
                                            • Recognize that Quality is more than just meeting our client’s
                                            basic needs and requirements<br>
                                            • Continuous education of personnel with Quality Service in mind<br>
                                            • Work as a TEAM with our Clients to develop Partnerships and
                                            Strategic Alliances<br>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accrodion">
                            <div class="accrodion-title">
                                <h4>Our Quality Policy</h4>
                            </div>
                            <div class="accrodion-content">
                                <div class="img-caption">
                                    <div class="img-box">
                                        <img src="images/vessels/policy.jpg" alt="Awesome Image"/>
                                    </div>
                                    <div class="content-box">
                                        <p>Quality Service is the means of achieving and exceeding the required standards to satisfy the needs and to ensure strict adherence to our Clients requirements for a safe, efficient and cost-effective operation. All of our client commitments, supporting actions and services delivered must be recognized as an expression of CANEGA’s image and its Quality System. We pledge to monitor our performance because to CANEGA; <b>“Quality is a continuous never ending commitment to improvement”.</b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="sec-title">
                        <h2><span>Oil & Gas Review</span></h2>
                    </div>
                    <div class="call-to-action-text">
                        <style>
                            .lity-content{height:700px;}
                        </style>
                        <a href="/images/team/mexico2014.pdf" data-lity>
                            <img src="images/team/mexico2014.jpg" alt="Service Locations"/>
                        </a>
                        <p><i>Click read our article in the 2014 Mexico Oil & Gas Review</i></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--<section class="welcome-text" >--}}
        {{--<div class="thm-container">--}}
            {{--<div class="title-box text-right">--}}
                {{--<h3 >We Are <br>Trace Certified</h3>--}}
            {{--</div>--}}
            {{--<div class="text-box">--}}
                {{--<div class="col-md-10">--}}
                    {{--<h2 style="color:#fff;float:left;">We are an internationally recognized company committed to raising anti-bribery compliance standards worldwide.</h2>--}}
                {{--</div>--}}
                {{--<div class="col-md-2">--}}
                    {{--<img src="/images/certifications/trace.png" style="width:100%;"/>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
    {{--<section class="container">--}}
        {{--<a href="/companies">--}}
            {{--<img src="/images/companies.png" class="center-block" width="100%" style="padding:80px;"/>--}}
        {{--</a>--}}
    {{--</section>--}}
@endsection