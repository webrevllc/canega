<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="UTF-8">
    <title>Canega Group - The Sea is Our World</title>
    <!-- viewport meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- styles -->
    <!-- helpers -->
    <link rel="stylesheet" href="/plugins/bootstrap/css/bootstrap.min.css">
    <!-- fontawesome css -->
    <link rel="stylesheet" href="/plugins/font-awesome/css/font-awesome.min.css">
    <!-- strock gap icons -->
    <link rel="stylesheet" href="/plugins/Stroke-Gap-Icons-Webfont/style.css">
    <!-- revolution slider css -->
    <link rel="stylesheet" href="/plugins/revolution/css/settings.css">
    <link rel="stylesheet" href="/plugins/revolution/css/layers.css">
    <link rel="stylesheet" href="/plugins/revolution/css/navigation.css">
    <link rel="stylesheet" href="/plugins/flaticon/flaticon.css">
    <link href="/plugins/jquery-ui-1.11.4/jquery-ui.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css?31232">
    <link rel="stylesheet" href="/css/responsive.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Raleway%3A400%2C700%2C800" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Raleway%3A700%2C800" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" type="text/css" href="/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/lity.min.css">
    <link rel="stylesheet" type="text/css" href="/css/settings.css">
    <script type="text/javascript" src="/js/lity.min.js"></script>


    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <style type="text/css">	#rev_slider_14_1_wrapper .tp-loader.spinner2{ background-color: #FFFFFF !important; } </style>
    <style type="text/css">#rev_slider_14_1 .uranus.tparrows{width:50px; height:50px; background:rgba(255,255,255,0)}#rev_slider_14_1 .uranus.tparrows:before{width:50px; height:50px; line-height:50px; font-size:40px; transition:all 0.3s;-webkit-transition:all 0.3s}#rev_slider_14_1 .uranus.tparrows:hover:before{opacity:0.75}</style>

    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.video.min.js"></script>
    <script type="text/javascript">function setREVStartSize(e){
            try{ var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
                if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
            }catch(d){console.log("Failure at Presize of Slider:"+d)}
        };
    </script>
@yield('header')

</head>
<body ng-app="myApp">

@include('header')

@yield('content')
<footer id="footer" class="sec-padding">
    <div class="thm-container">
        <div class="row">
            <div class="col-md-3 col-xs-12  footer-widget">
                <div class="about-widget">
                    <a href="/#"><img src="/images/logos/Canega-Group-wSlogan2.png" alt="" width="90%" class="center-block"></a>
                </div>
            </div>
            <div class="col-xs-12 col-md-5 footer-widget">
                <div class="title">
                    <h3><span>Quick Links</span></h3>
                </div>
                <div class="col-xs-12 col-md-6">
                    <ul class="link-list">
                        <li><a href="/">Home</a></li>
                        <li><a href="/about">About Us</a></li>
                        <li><a href="/services">Services</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-6">
                    <ul class="link-list">
                        {{--<li><a href="/fleet">Our Fleet</a></li>--}}
                        <li><a href="/locations">Service Areas</a></li>
                        <li><a href="/contact">Contact Us</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-xs-12 col-md-4 col-sm-6 footer-widget">
                <div class="title">
                    <h3><span>Quick Contact</span></h3>
                </div>
                <ul class="contact-infos">
                    <li>
                        <div class="icon-box">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="text-box">
                            <p><b>Canega Group </b> <br> 27212 Breakers Drive, Wesley Chapel, FL 33544</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-box">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="text-box">
                            <p> (813) 907-3376</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-box">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="text-box">
                            <p>Chartering Dept: chartering@canega.com</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-box">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="text-box">
                            <p>Port Agency Dept: canops@canega.com</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<section class="bottom-bar">
    <div class="thm-container clearfix">
        <div class="pull-left">
            <p>Copyright © Canega Inc. {{ \Carbon\Carbon::now()->year }}. All rights reserved.</p>
        </div>
        <div class="pull-right">
            <p>Created by: <a href="mailto:webrevllc@gmail.com">WebRev LLC</a></p>
        </div>
    </div>
</section>


<script src="/js/main.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>
    $('.carousel').carousel()
</script>

@yield('footer')

</body>
</html>