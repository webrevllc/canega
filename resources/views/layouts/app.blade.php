<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Canega - The Sea is Our World</title>
    <!-- viewport meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- styles -->
    <!-- helpers -->
    <link rel="stylesheet" href="/plugins/bootstrap/css/bootstrap.min.css">
    <!-- fontawesome css -->
    <link rel="stylesheet" href="/plugins/font-awesome/css/font-awesome.min.css">
    <!-- strock gap icons -->
    <link rel="stylesheet" href="/plugins/Stroke-Gap-Icons-Webfont/style.css">
    <!-- revolution slider css -->
    <link rel="stylesheet" href="/plugins/revolution/css/settings.css">
    <link rel="stylesheet" href="/plugins/revolution/css/layers.css">
    <link rel="stylesheet" href="/plugins/revolution/css/navigation.css">
    <!-- flaticon css -->
    <link rel="stylesheet" href="/plugins/flaticon/flaticon.css">
    <!-- jQuery ui css -->
    <link href="/plugins/jquery-ui-1.11.4/jquery-ui.css" rel="stylesheet">
    <!-- owl carousel css -->
    <link rel="stylesheet" href="/plugins/owl.carousel-2/assets/owl.carousel.css">
    <link rel="stylesheet" href="/plugins/owl.carousel-2/assets/owl.theme.default.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="/plugins/animate.min.css">
    <!-- fancybox -->
    <link rel="stylesheet" href="/plugins/fancyapps-fancyBox/source/jquery.fancybox.css">



    <!-- master stylesheet -->
    <link rel="stylesheet" href="/css/style.css?123">
    <!-- responsive stylesheet -->
    <link rel="stylesheet" href="/css/responsive.css">

    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#061E2A">

</head>
<body>

<section id="top-bar">
    <div class="thm-container clearfix">
        <div class="phone pull-left">
            <p>Call Us Now: <span>(813) 907-3376</span></p>
        </div>
        <div class="top-contact-info pull-right">
            <ul>
                <li><i class="fa fa-map-marker"></i> 27212 Breakers Drive, Wesley Chapel, FL</li>
                <li><i class="fa fa-envelope"></i>  chartering@canega.com</li>
            </ul>
        </div>
    </div>
</section>

<header id="header3" class="stricky">
    <div class="thm-container clearfix">
        <div class="logo pull-left">
            <a href="/">
                <img src="/images/logos/header-logo.png" alt="">
            </a>
        </div>
        <div class="nav-holder pull-right">
            <div class="nav-header">
                <button><i class="fa fa-bars"></i></button>
            </div>
            <div class="nav-footer">
                <ul class="nav">
                    <li >
                        <a href="/">Home</a>
                    </li>
                    <li class="has-submenu">
                        <a href="#">about us</a>
                        <ul class="submenu">
                            <li><a href="/about">About Canega</a></li>
                            <li><a href="/recommendations">Letters of Recommendation</a></li>
                            <li><a href="/team">Our Team</a></li>
                            <li><a href="/company-history">Company History</a></li>
                            <li><a href="/clients">Our Clients</a></li>
                            <li><a href="/testimonials">Testimonials</a></li>
                            <li><a href="/career">Career</a></li>
                        </ul>
                    </li>
                    <li >
                        <a href="/services">Services</a>
                        {{--<ul class="submenu">--}}
                        {{--<li><a href="/single-service">Service Details</a></li>--}}
                        {{--</ul>--}}
                    </li>
                    <li >
                        <a href="/fleet">Our Fleet</a>
                    </li>


                    <li><a href="/contact">contact us</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>
@yield('content')

<section class="footer-top">
    <div class="thm-container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <h3>The Sea is Our World</h3>
                    <p>Contact us now to get quote for all your global <br>shipping and cargo need.</p>
                </div>
                <div class="col-md-6">
                    <a class="thm-btn" href="/contact">Contact Us <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>


<footer id="footer" class="sec-padding">
    <div class="thm-container">
        <div class="row">
            <div class="col-md-3 col-sm-6 footer-widget">
                <div class="about-widget">
                    <a href="/#"><img src="/images/logos/bwlogo.png" alt="" ></a>
                    {{--<a href="/#">Read More <i class="fa fa-angle-double-right"></i></a>--}}
                    {{--<ul class="social">--}}
                    {{--<li><a href="/#"><i class="fa fa-facebook"></i></a></li>--}}
                    {{--<li><a href="/#"><i class="fa fa-twitter"></i></a></li>--}}
                    {{--<li><a href="/#"><i class="fa fa-google-plus"></i></a></li>--}}
                    {{--<li><a href="/#"><i class="fa fa-linkedin"></i></a></li>--}}
                    {{--</ul>--}}
                </div>
            </div>
            <div class="col-md-3 col-sm-6 footer-widget">
                <div class="pl-30">
                    <div class="title">
                        <h3><span>Our Services</span></h3>
                    </div>
                    <ul class="link-list">
                        <li><a href="/#">Sea Freight</a></li>
                        <li><a href="/#">Road Transportation</a></li>
                        <li><a href="/#">Air Freight</a></li>
                        <li><a href="/#">Railway Logistics</a></li>
                        <li><a href="/#">Packaging & Storage</a></li>
                        <li><a href="/#">Warehousing</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2 col-sm-6 footer-widget">
                <div class="title">
                    <h3><span>Quick Links</span></h3>
                </div>
                <ul class="link-list">
                    <li><a href="/">Home</a></li>
                    <li><a href="/services">Our Services</a></li>
                    <li><a href="/about">About Us</a></li>
                    <li><a href="/fleet">Our Fleet</a></li>
                    <li><a href="/contact">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-6 footer-widget">
                <div class="title">
                    <h3><span>Quick Contact</span></h3>
                </div>
                <ul class="contact-infos">
                    <li>
                        <div class="icon-box">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="text-box">
                            <p><b>Canega Group Inc.</b> <br> 27212 Breakers Drive, Wesley Chapel, FL 33544</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-box">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="text-box">
                            <p> (813) 907-3376</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-box">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="text-box">
                            <p>Chartering Dept: chartering@canega.com</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-box">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="text-box">
                            <p>Port Agency Dept: canops@canega.com</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<section class="bottom-bar">
    <div class="thm-container clearfix">
        <div class="pull-left">
            <p>Copyright © Canega Inc. {{date('Y')}}. All rights reserved.</p>
        </div>
        <div class="pull-right">
            <p>Created by: <a href="/http://webrevllc.com">WebRev LLC</a></p>
        </div>
    </div>
</section>


<!-- jQuery js -->
<script src="/plugins/jquery/jquery-1.11.3.min.js"></script>
<!-- bootstrap js -->
<script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- jQuery ui js -->
<script src="/plugins/jquery-ui-1.11.4/jquery-ui.js"></script>
<!-- owl carousel js -->
<script src="/plugins/owl.carousel-2/owl.carousel.min.js"></script>
<!-- jQuery appear -->
<script src="/plugins/jquery-appear/jquery.appear.js"></script>
<!-- jQuery countTo -->
<script src="/plugins/jquery-countTo/jquery.countTo.js"></script>
<!-- jQuery validation -->
<script src="/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<!-- gmap.js helper -->
<script src="http://maps.google.com/maps/api/js"></script>
<!-- gmap.js -->
{{--<script src="/plugins/gmap.js"></script>--}}
<!-- mixit up -->
<script src="/plugins/jquery.mixitup.min.js"></script>

<!-- revolution slider js -->
<script src="/plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="/plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>

<!-- fancy box -->
<script src="/plugins/fancyapps-fancyBox/source/jquery.fancybox.pack.js"></script>
<!-- type script -->
<script src="/plugins/typed.js-master/dist/typed.min.js"></script>

<!-- theme custom js  -->
<script src="/js/main.js"></script>



</body>
</html>