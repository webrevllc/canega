@extends('layout')

@section('content')
    <section class="inner-banner areas">
        <div class="thm-container">
            <h2 class="text-center">Our Service Areas</h2>
        </div>
    </section>

    <section class="fleet-gallery blog-page single-post-page" style="padding: 40px;">
        <div class="container">
            <div class="col-md-7" >
                <div class="hovereffect">
                    <img src="/images/locations/america.png" class="img-responsive" style="width:100%;"/>
                    <div class="overlay">
                        <h2>U.S.A.</h2>
                        <a class="info" href="/images/locations/america.png" data-lity>Zoom</a>
                    </div>
                </div>
                <div class="hovereffect">
                    <img src="/images/locations/mexico.png" class="img-responsive" style="width:100%;"/>
                    <div class="overlay">
                        <h2>Mexico</h2>
                        <a class="info" href="/images/locations/mexico.png" data-lity>Zoom</a>
                    </div>
                </div>
                <div class="hovereffect">
                    <img src="/images/locations/central-america.png" class="img-responsive" style="width:100%;"/>
                    <div class="overlay">
                        <h2>Central America</h2>
                        <a class="info" href="/images/locations/central-america.png" data-lity>Zoom</a>
                    </div>
                </div>
                <div class="hovereffect">
                    <img src="/images/locations/sae.png" class="img-responsive" style="width:100%;"/>
                    <div class="overlay">
                        <h2>United Arab Emirates</h2>
                        <a class="info" href="/images/locations/sae.png" data-lity>Zoom</a>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="hovereffect">
                    <img src="/images/locations/samerica.png" class="img-responsive" style="width:100%;"/>
                    <div class="overlay">
                        <h2>South America</h2>
                        <a class="info" href="/images/locations/samerica.png" data-lity>Zoom</a>
                    </div>
                </div>
                <div class="hovereffect">
                    <img src="/images/locations/carribean.png" class="img-responsive" style="width:100%;"/>
                    <div class="overlay">
                        <h2>Caribbean</h2>
                        <a class="info" href="/images/locations/carribean.png" data-lity>Zoom</a>
                    </div>
                </div>
                <div class="hovereffect">
                    <img src="/images/locations/wafrica.png?1" class="img-responsive" style="width:100%;"/>
                    <div class="overlay">
                        <h2>West Africa</h2>
                        <a class="info" href="/images/locations/wafrica.png?1" data-lity>Zoom</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection