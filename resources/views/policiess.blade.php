@extends('layout')

@section('content')


    <section class="inner-banner about">
        <div class="thm-container">
            <h2 class="text-center" style="text-shadow: 1px 2px 4px #000">Our Policies</h2>
        </div>
    </section>

    <section class=" page-title">
        <div class="thm-container" style="min-height: 450px;">

            <br>
            <br>
            <div class="">
                <div class="sec-title">
                    <h2><span>Español</span></h2>
                </div>
            </div>

            <div class="">
                <table class="table table-bordered">
                    <tr>
                        <td class="col-md-10"><h3>Codigo Etico</h3></td>
                        <td class="col-md-2"><a href="{{asset('policies/spanish/CANEGA - CODIGO ETICO.pdf')}}" class="btn btn-block btn-info" target="_blank"><i class="fa fa-eye"></i>   View</a></td>
                    </tr>
                    <tr>
                        <td class="col-md-10"><h3>{{ucwords(strtolower('POLITICA DE ALCOHOL Y DROGAS'))}}</h3></td>
                        <td class="col-md-2"><a href="{{asset('policies/spanish/CANEGA - POLITICA DE ALCOHOL Y DROGAS.pdf')}}" class="btn btn-block btn-info" target="_blank"><i class="fa fa-eye"></i>   View</a></td>
                    </tr>
                    <tr>
                        <td class="col-md-10"><h3>{{ucwords(strtolower('POLITICA DE CALIDAD'))}}</h3></td>
                        <td class="col-md-2"><a href="{{asset('policies/spanish/CANEGA - POLITICA DE CALIDAD.pdf')}}" class="btn btn-block btn-info" target="_blank"><i class="fa fa-eye"></i>   View</a></td>
                    </tr>
                    <tr>
                        <td class="col-md-10"><h3>{{ucwords(strtolower('POLITICA DE MEDIO AMBIENTE'))}}</h3></td>
                        <td class="col-md-2"><a href="{{asset('policies/spanish/CANEGA - POLITICA DE MEDIO AMBIENTE.pdf')}}" class="btn btn-block btn-info" target="_blank"><i class="fa fa-eye"></i>   View</a></td>
                    </tr>
                    <tr>
                        <td class="col-md-10"><h3>{{ucwords(strtolower('POLITICA DE SEGURIDAD Y SALUD OCUPACIONAL'))}}</h3></td>
                        <td class="col-md-2"><a href="{{asset('policies/spanish/CANEGA - POLITICA DE SEGURIDAD Y SALUD OCUPACIONAL.pdf')}}" class="btn btn-block btn-info" target="_blank"><i class="fa fa-eye"></i>   View</a></td>
                    </tr>
                </table>
            </div>
            <br>
            <br>
            <br>
            <div class="">
                <div class="sec-title">
                    <h2><span>English</span></h2>
                </div>
            </div>
            <div class="">
                <table class="table table-bordered">
                    <tr>
                        <td class="col-md-10"><h3>Code of Conduct</h3></td>
                        <td class="col-md-2"><a href="{{asset('policies/english/CANEGA GROUP CODE OF CONDUCT.pdf')}}" class="btn btn-block btn-info" target="_blank"><i class="fa fa-eye"></i>   View</a></td>
                    </tr>
                    <tr>
                        <td class="col-md-10"><h3>Anti-Corruption, Anti-Bribery Company Policy</h3></td>
                        <td class="col-md-2"><a href="{{asset('policies/english/Canega Group Anti.pdf')}}" class="btn btn-block btn-info" target="_blank"><i class="fa fa-eye"></i>   View</a></td>
                    </tr>
                    <tr>
                        <td class="col-md-10"><h3>Environmental, Health and Safety Policy</h3></td>
                        <td class="col-md-2"><a href="{{asset('policies/english/Environmental, Health and Safety Policy.pdf')}}" class="btn btn-block btn-info" target="_blank"><i class="fa fa-eye"></i>   View</a></td>
                    </tr>
                    <tr>
                        <td class="col-md-10"><h3>Ethics and Corporate Social Responsibility Policy</h3></td>
                        <td class="col-md-2"><a href="{{asset('policies/english/Ethics and Corporate Social Responsibility Policy.pdf')}}" class="btn btn-block btn-info" target="_blank"><i class="fa fa-eye"></i>   View</a></td>
                    </tr>
                </table>
            </div>
        </div>
    </section>





@endsection