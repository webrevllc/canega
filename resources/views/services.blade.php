@extends('layout')

@section('content')


    <section class="inner-banner service">
        <div class="thm-container">
            <h2 class="text-center">Our Services</h2>
            <br>
        </div>
    </section>


    <section class="team-section sec-padding team-page" style="margin-top:50px;">
        <div class="thm-container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="single-team-member img-cap-effect">
                        <div class="img-box">
                            <img src="/images/vessels/tanker2.jpg" alt="Awesome Image"  class="center-block" width="100%" height="250"/>
                        </div>
                        <div class="content">
                            <div class="name-box">
                                <h3>Agency Department Tanker Division</h3>
                            </div>
                            <p>Crude Oil, Clean Product & Chemical Tankers, LNG and LPG Carriers...</p>
                        </div>
                        <a href="/services/tankers" class="readmore">Read More <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-team-member img-cap-effect">
                        <div class="img-box">
                            <img src="/images/services/offshore.png" alt="Awesome Image"  class="center-block" width="100%"/>
                        </div>
                        <div class="content">
                            <div class="name-box">
                                <h3>Agency Department Offshore Oil & Gas Division</h3>
                            </div>
                            <p>From Agency Services to Emergency Response Planning...</p>
                        </div>
                        <a href="/services/offshore"  class="readmore">Read More <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-team-member img-cap-effect">
                        <div class="img-box">
                            <img src="/images/vessels/customs/projecthandling1.jpg" alt="Customs Broker" class="center-block" width="100%" height="250px"/>
                        </div>
                        <div class="content">
                            <div class="name-box">
                                <h3>CUSTOMS BROKER AND FREIGHT
                                    FORWARDING SERVICES</h3>
                            </div>
                            <p>For over 30 years, Canega Group and Central Dispatch... </p>
                        </div>
                        <a href="/services/cdi" class="readmore">Read More <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="single-team-member img-cap-effect">
                        <div class="img-box">
                            <img src="/images/services/diving.png" alt="Awesome Image"  class="center-block" width="100%"/>
                        </div>
                        <div class="content">
                            <div class="name-box">
                                <h3>IMCA CERTIFIED DIVING SERVICES</h3>
                            </div>
                            <p>Canega Offshore Services will become the first company in Mexico...</p>
                        </div>
                        <a href="/services/diving" class="readmore">Read More <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="single-team-member img-cap-effect">
                        <div class="img-box">
                            <img src="/images/services/rov.png" alt="Awesome Image"  class="center-block" width="100%" />
                        </div>
                        <div class="content">
                            <div class="name-box">
                                <h3>ROV INSPECTIONS</h3>
                            </div>
                            <p>Canega Offshore Services will provide ROV Services in Mexico...</p>
                            <br>
                        </div>
                        <a href="/services/rov" class="readmore">Read More <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-team-member img-cap-effect">
                        <div class="img-box">
                            <img src="/images/services/savante.png" alt="Awesome Image" class="center-block" width="100%" height="250"/>
                        </div>
                        <div class="content">
                            <div class="name-box">
                                <h3>INSPECTION SERVICES</h3>
                            </div>
                            <p>Canega Offshore Services will provide
                                inspection services...</p>
                        </div>
                        <a href="/services/inspections" class="readmore">Read More <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-team-member img-cap-effect">
                        <div class="img-box">
                            <img src="/images/services/chandler5.jpg" alt="Awesome Image" class="center-block" width="100%"  height="250"/>
                        </div>
                        <div class="content">
                            <div class="name-box">
                                <h3>Ship Chandler & Provisions</h3>
                            </div>
                            <p>We have successfully met the challenges of providing these companies with...</p>
                        </div>
                        <a href="/services/chandler" class="readmore">Read More <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-team-member img-cap-effect">
                        <div class="img-box">
                            <img src="/images/services/brokering.png" alt="Awesome Image" class="center-block" width="100%"/>
                        </div>
                        <div class="content">
                            <div class="name-box">
                                <h3>SHIP BROKERING & CHARTERING</h3>
                            </div>
                            <p>Our Ship Brokering and Chartering Department has assisted companies... </p>
                        </div>
                        <a href="/services/brokering" class="readmore">Read More <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>


            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="single-team-member img-cap-effect">
                        <div class="img-box">
                            <img src="/images/services/catering.png" alt="Awesome Image" class="center-block" width="100%"/>
                        </div>
                        <div class="content">
                            <div class="name-box">
                                <h3>HOTEL & CATERING SERVICE</h3>
                            </div>
                            <p>Canega Offshore Services will provide first class Hotel & Catering Services...</p>
                        </div>
                        <a href="/services/catering" class="readmore">Read More <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-team-member img-cap-effect">
                        <div class="img-box">
                            <img src="/images/services/chain.png" alt="Awesome Image" class="center-block" width="100%"/>
                        </div>
                        <div class="content">
                            <div class="name-box">
                                <h3>SUPPLY CHAIN MGMT</h3>
                            </div>
                            <p>Canega Group is
                                presently developing the first
                                Supply Chain Management...</p>
                        </div>
                        <a href="/services/supplychain" class="readmore">Read More <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-team-member img-cap-effect">
                        <div class="img-box">
                            <img src="/images/services/erp.png" alt="Awesome Image" class="center-block" width="100%"/>
                        </div>
                        <div class="content">
                            <div class="name-box">
                                <h3>EMERGENCY MEDICAL EVACUATION PLAN</h3>
                            </div>
                            <p>Canega Group since 1983 has maintained an Emergency...</p>
                        </div>
                        <a href="/services/erp" class="readmore">Read More <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-team-member img-cap-effect">
                        <div class="img-box">
                            <img src="/images/services/pi.jpg" alt="Awesome Image" class="center-block" width="100%" height="240px"/>
                        </div>
                        <div class="content">
                            <div class="name-box">
                                <h3>Platform Installation and Pipe Laying (W. Africa)</h3>
                            </div>
                            <p>Canega Offshore Services, Grupo Protexa and CNS Marine Nigeria...</p>
                        </div>
                        <a href="/services/pipe" class="readmore">Read More <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="single-team-member img-cap-effect">
                        <div class="img-box">
                            <img src="/images/services/geo1.jpg" alt="Awesome Image" class="center-block" width="100%" height="240px"/>
                        </div>
                        <div class="content">
                            <div class="name-box">
                                <h3>Positioning, Geophysical and Geotechnical Services</h3>
                            </div>
                            <p>Canega, with an exclusive partnership with Capital Signal... </p>
                        </div>
                        <a href="/services/positioning" class="readmore">Read More <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection