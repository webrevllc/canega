@extends('layout')

@section('content')
    <section class="inner-banner service">
        <div class="thm-container">
            <h2 class="text-center">SHIP BROKERING & CHARTERING</h2>
            <br>
        </div>
    </section>
    <section class="blog-page single-post-page ">
        <div class="thm-container">
            <div class="row">
                <div class="col-md-12 pull-left">

                    <div class="single-post-wrapper">
                        <article class="single-blog-post img-cap-effect">
                            <br>
                            <h3>
                                Our Ship Brokering and Chartering Department has assisted companies around the world to find the right vessel(s) to meet their specific requirement(s).
                            </h3>
                            <br>
                            <div class="img-box">
                                <img src="/images/services/brokeringimg.PNG" alt="Awesome Image" class="center-block" width="100%"/>
                            </div>
                            <div class="row">

                                <div class="col-md-6 col-md-offset-3 text-center">
                                    <h3 class="cdi"><i class="fa fa-circle bb"></i> DSV – Diving Support Vessels</h3>
                                    <h3 class="cdi"><i class="fa fa-circle bb"></i> Seismic Vessels</h3>
                                    <h3 class="cdi"><i class="fa fa-circle bb"></i> MPSV – Multi Purpose Service Vessels</h3>
                                    <h3 class="cdi"><i class="fa fa-circle bb"></i> HLV – Heavy Lift Vessels</h3>
                                    <h3 class="cdi"><i class="fa fa-circle bb"></i> PSV – Platform Supply Vessels</h3>
                                    <h3 class="cdi"><i class="fa fa-circle bb"></i> AHTS – circle Handling Supply Vessels</h3>
                                    <h3 class="cdi"><i class="fa fa-circle bb"></i> FISV – Fast Intervention Supply Vessels</h3>
                                    <h3 class="cdi"><i class="fa fa-circle bb"></i> Offshore Tugs</h3>
                                    <h3 class="cdi"><i class="fa fa-circle bb"></i> Barges</h3>
                                </div>
                            </div>
                            <br>
                            <br>
                        </article>

                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection