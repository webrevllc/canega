@extends('layout')

@section('content')
    <section class="inner-banner service">
        <div class="thm-container">
            <h2 class="text-center">HOTEL & CATERING SERVICES</h2>
            <br>
        </div>
    </section>
    <section class="blog-page single-post-page ">
        <div class="thm-container">
            <div class="row">
                <div class="col-md-12 pull-left">

                    <div class="single-post-wrapper">
                        <article class="single-blog-post img-cap-effect">

                            <div class="row">
                                <br>
                                <br>
                                <br>
                                <br>
                                <div class="col-md-12" >
                                    <h3 class="text-left">
                                        Canega Offshore Services, in close cooperation with our International Oil Companies clients around the world, have identified and partnered with on exclusive basis, the best Hotel and Catering Service Companies to provide and meet our Client’s requirements onboard our vessel’s operating around the world.
                                    </h3>
                                    <br/>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="img-box">
                                        <img src="/images/services/cateringimg.PNG" alt="Awesome Image" class="center-block"/>
                                    </div>
                                </div>

                            </div>
                            <br>
                            <br>
                        </article>

                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection