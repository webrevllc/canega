@extends('layout')

@section('content')
    <section class="inner-banner service">
        <div class="thm-container">
            <h2 class="text-center">CUSTOMS BROKER AND FREIGHT <br>FORWARDING SERVICES</h2>
        </div>
    </section>
    <section class="blog-page single-post-page ">
    <div class="thm-container">
        <div class="row">
            <div class="col-md-12 pull-left">

                <div class="single-post-wrapper">
                    <article class="single-blog-post img-cap-effect">

                        <div class="row">
                            <div class="col-md-12" style="margin-top:50px;">
                                <h3 class="text-justify">For over 30 years, Canega Group and Central Dispatch have worked together on an exclusive partnership to provide
                                    clients in the US and Mexico with a seamless and efficient operation in the freight forwarding and importation/exportation of cargo and equipment from and into the US and Mexico. </h3>
                                <br>
                             </div>
                         </div>
                        <div class="row" style="padding:20px;margin:20px;">
                            <div class="col-md-3">
                                <img src="/images/vessels/customs/4776a.jpg" width="100%" height="200px"/>
                            </div>
                            <div class="col-md-3">
                                <img src="/images/vessels/customs/4838a.jpg" width="100%" height="200px"/>
                            </div>
                            <div class="col-md-3">
                                <img src="/images/vessels/customs/4839a.jpg" width="100%" height="200px"/>
                            </div>
                            <div class="col-md-3">
                                <img src="/images/vessels/customs/projecthandling1.jpg" width="100%" height="200px"/>
                            </div>
                        </div>
                        <div class="row">
                            {{--<img src="/images/logos/centraldispatch.png"  class="center-block" style="padding:50px;"/>--}}

                            <h3 style="margin-left:11px;"><i>Our combined services in the US and Mexico include:</i></h3>
                            <br>
                            <h3 class="cdi"><i class="fa fa-circle bb"></i> Canega Group and CDI are International Freight Forwarders.</h3>
                            <h3 class="cdi"><i class="fa fa-circle bb"></i> Seamless Customs Brokering and International Freight Forwarding Services from any port in USA to Mexico.</h3>
                            <h3 class="cdi"><i class="fa fa-circle bb"></i> Canega Group and CDI combined offices are located in Houston, New Orleans in the United States and in Ciudad
                                del Carmen, Dos Bocas, Coatzacoalcos, Tuxpan, Tampico, Matamoros, McAllen, Laredo, Mexico</h3>
                            <h3 class="cdi"><i class="fa fa-circle bb"></i> CDI is a Custom Broker approved for remote filing able to make entry at any port in the USA.</h3>
                            <h3 class="cdi"><i class="fa fa-circle bb"></i> CDI presently has Warehouses (Bonded & Domestic) for receipt, consolidating, packing and / or loading materials
                                for export. Canega Group is working to obtain and secure permits from the Mexican Government to have bonded
                                warehouses.</h3>
                            <h3 class="cdi"><i class="fa fa-circle bb"></i> CDI and Canega Group have available a trucking division for the delivery of domestic and bonded merchandise.</h3>
                            <h3 class="cdi"><i class="fa fa-circle bb"></i> Canega Group is developing and will offer Marine and Crew Transportation Services.</h3>

                        </div>
                    </article>

                </div>

            </div>
        </div>
    </div>
</section>
@endsection