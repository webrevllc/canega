@extends('layout')

@section('content')
    <section class="inner-banner service">
        <div class="thm-container">
            <h2 class="text-center">Ship Chandler & Provisions</h2>
            <br>
        </div>
    </section>
    <section class="blog-page single-post-page" style="margin-top:50px;">
        <div class="thm-container">
            <div class="row">
                <div class="col-md-12 pull-left">

                    <div class="single-post-wrapper">
                        <article class="single-blog-post img-cap-effect">
                            <div class="row">
                                <h4>
                                    Since 2006 Canega Group, through its affilate BNP del Golfo has been providing first class quality ship Chandling and provision services to a large group of Offshore companies operating in the Bay of Campeche.
                                    <br>
                                    <br>
                                    Our client base includes Mexican, American, European, Scandinavian, and Southeast Asia companies. We have successfully met the challenges of providing these companies with their requirement of specialty foods.</h4>
                            </div>
                            <div class="row">
                                <br/>
                                <div class="col-md-6">
                                    <img src="/images/services/chandler2.jpg" width="100%" height="500px"/>
                                </div>
                                <div class="col-md-6">
                                    <img src="/images/services/chandler3.jpg" width="100%" height="500px"/>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row" style="padding-top:30px;">
                                <div class="col-md-3 text-center">

                                </div>
                                <div class="col-md-3 text-center">

                                </div>
                                <div class="col-md-3 text-center">

                                </div>
                                <div class="col-md-3 text-center">

                                </div>
                            </div>
                            <br>
                        </article>

                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection