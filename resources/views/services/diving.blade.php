@extends('layout')

@section('content')
    <section class="inner-banner service">
        <div class="thm-container">
            <h2 class="text-center">IMCA CERTIFIED DIVING SERVICES</h2>
            <br>
        </div>
    </section>
    <section class="blog-page single-post-page " style="margin-top:50px;">
        <div class="thm-container">
        <div class="row">
            <div class="col-md-12 pull-left">
                <div class="single-post-wrapper">
                    <article class="single-blog-post img-cap-effect">
                        <div class="row">
                            <div class="col-md-6" >
                                <div class="img-box">
                                    <img src="/images/services/imca.jpg" alt="Awesome Image"/>
                                </div>
                                <br>
                                <h3>Canega Offshore Services in an exclusive
                                    partnership with CNS International SRL,
                                    will become the First Company in Mexico
                                    to provide IMCA Certified Saturation and
                                    Surface Diving Services.
                                </h3>
                                <br>
                                <h3>
                                    CNS International SRL, is a well-established
                                    organization, capable of providing turnkey
                                    diving services worldwide, in particular for
                                    construction and IMR offshore projects.</h3>
                            </div>
                            <div class="col-md-5 col-md-offset-1">
                                <img src="/images/services/diving.jpg" width="100%" height="375px"/>
                            </div>
                        </div>

                        <div class="row well well-lg" style="margin-top:40px;">
                            <h2 class="text-center">Main Capabilities Include:</h2>
                            <br>
                            <div class="col-md-6 text-left">
                                <h3><i class="fa fa-circle bb"></i> Saturation Diving services up to -300 msw</h3>
                                <h3><i class="fa fa-circle bb"></i> Air/Mixed Gas diving services up to -50 msw,</h3>
                                <h3><i class="fa fa-circle bb"></i> Subsea engineering</h3>
                                <h3><i class="fa fa-circle bb"></i> Diving system engineering</h3>
                                <h3><i class="fa fa-circle bb"></i> Consulting and Auditing services</h3>
                            </div>
                            <div class="col-md-6 text-left">
                                <h3><i class="fa fa-circle bb"></i> Third party diving systems intervention:</h3>
                                <h3><i class="fa fa-circle bb"></i> Commissioning</h3>
                                <h3><i class="fa fa-circle bb"></i> Maintenance</h3>
                                <h3><i class="fa fa-circle bb"></i> Classification /Certification management</h3>
                                <h3><i class="fa fa-circle bb"></i> Data Gathering / Reporting with dedicated software</h3>
                            </div>
                        </div>
                        <br>
                        <br>
                    </article>

                </div>

            </div>
        </div>
    </div>
    </section>
@endsection