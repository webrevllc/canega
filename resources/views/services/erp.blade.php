@extends('layout')

@section('content')
    <section class="inner-banner service">
        <div class="thm-container">
            <h2 class="text-center">EMERGENCY MEDICAL EVACUATION PLAN</h2>
            <br>
        </div>
    </section>
    <section class="blog-page single-post-page" style="margin-top:50px;">
        <div class="thm-container">
            <div class="row">
                <div class="col-md-12 pull-left">

                    <div class="single-post-wrapper">
                        <article class="single-blog-post img-cap-effect">
                            <div class="row">
                                    <h3 class="text-justify">
                                        Canega Group since 1983 has maintained an Emergency Response Plan in effect
                                        to rapidly respond to any Medical Emergency in the areas serviced by Canega.
                                        <br>
                                        <br>
                                        Since 1983, we had to activate our Emergency Response Plan several times for MedEvac from
                                        Mexico, Venezuela and Nicaragua to Memorial Hermann Hospital in Houston, Texas.
                                        <br>
                                        <br>
                                        Canega Group has agreements with local helicopter companies to assist in the evacuation of injured
                                        personnel onboard a vessel or platform in the Bay of Campeche. We are presently revising our
                                        Emergency Response Plan in preparation for future needs based on Mexico’s Energy Reform.
                                    </h3>
                                    <br>
                                    <br>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="img-box">
                                        <img src="/images/services/erpimg.PNG" alt="Awesome Image" class="center-block" style="width:100%;"/>
                                    </div>
                                </div>

                            </div>
                            <br>
                            <br>
                        </article>

                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection