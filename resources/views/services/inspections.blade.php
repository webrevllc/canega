@extends('layout')

@section('content')
    <section class="inner-banner service">
        <div class="thm-container">
            <h2 class="text-center">INSPECTION SERVICES</h2>
            <br>
        </div>
    </section>
    <section class="blog-page single-post-page ">
        <div class="thm-container">
        <div class="row">
            <div class="col-md-12 pull-left">
                <div class="single-post-wrapper">

                    <article class="single-blog-post img-cap-effect">

                        <div class="row">
                            <br>
                            <div class="col-md-4 " >
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <h3 style="margin-left:-.7em;">Canega Offshore Services
                                    in an exclusive partnership
                                    with Savante will provide
                                    inspection services utilizing
                                    laser technology.
                                </h3>
                                <br>
                                <h3><i class="fa fa-circle bb"></i> 3D laser scanning</h3>
                                <h3><i class="fa fa-circle bb"></i> Photogrammetry</h3>
                                <h3><i class="fa fa-circle bb"></i> Stereo-imaging</h3>
                                <h3><i class="fa fa-circle bb"></i> SLAM (self-localisation and mapping)</h3>

                            </div>
                            <div class="col-md-8">
                                <div class="img-box">
                                    <img src="/images/services/savanteimg.PNG" alt="Awesome Image"/>
                                </div>
                            </div>

                        </div>
                        <br>
                        <br>
                    </article>

                </div>

            </div>
        </div>
    </div>
    </section>
@endsection