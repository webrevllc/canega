@extends('layout')

@section('content')
    <section class="inner-banner service">
        <div class="thm-container">
            <h2 class="text-center">AGENCY DEPARTMENT <br>OFFSHORE OIL & GAS DIVISION</h2>
        </div>
    </section>

    <section class="blog-page single-post-page ">
    <div class="thm-container">
        <div class="row">
            <div class="col-md-12 pull-left">

                <div class="single-post-wrapper">

                    <article class="single-blog-post img-cap-effect">
                        <div class="row" style="margin-top:50px;">
                            <h3>
                                We offer a full range of agency services from cargo operations, bunkering, husbandry and global protective agency services. We manage both spot calls and contractual agreements for ship and offshore assets.
                                <br>
                                <br>
                                Our agents, backed by more than 30 years of extensive agency experience and competence, provide the following services:
                            </h3>
                            <br>
                            <h3><i class="fa fa-circle bb"></i> <b>Full Port Agency</b> – Managing port and cargo operations as well as service procurement, reporting and accounting on behalf of ship owner and charterers.</h3>
                            <h3><i class="fa fa-circle bb"></i> <b>Protective Agency</b> – Representing ship owners’ and operators’ interests in port operations process when the charterer's nominated agent has been appointed.</h3>
                            <h3><i class="fa fa-circle bb"></i> <b>Husbandry Agency</b> – Managing service requirements for ship and crew, as well as bunker calls and transits.</h3>
                                <h3><i class="fa fa-circle bb"></i> <b>Cargo Agency </b>– Representing the cargo owners’ interests in the port either as the nominated agent or as the shippers/receivers agents for both wet and dry operations. Also involved in generating bill of lading.</h3>
                            <h3><i class="fa fa-circle bb"></i> <b>Crew support</b> – Arranging transport, accommodation and facilitating immigration and work visas.</h3>
                            <h3><i class="fa fa-circle bb"></i> <b>General Port Agency.</b></h3>
                            <h3><i class="fa fa-circle bb"></i> <b>Logistics Services</b> – arranging air freight, warehousing, distribution and customs clearance for ships spares, cranage, helicopter transportation, fuel services, navigation permits, geophysical and seismic surveys, and Geotechnical Investigations DGPS/RTK Services for Pipe-lay and Platform Approaches.</h3>
                        </div>
                        {{--<div class="img-box">--}}
                            {{--<img src="/images/services/offshoreimg.png?1" alt="Awesome Image" width="100%"/>--}}
                        {{--</div>--}}
                        <div class="row">
                            <br/>
                            <div class="col-md-3">
                                <img src="/images/vessels/Songa_Venus.jpg" width="100%" height="250px"/>
                            </div>
                            <div class="col-md-3">
                                <img src="/images/vessels/photo 10.jpg" width="100%" height="250px"/>
                            </div>
                            <div class="col-md-3">
                                <img src="/images/vessels/offshore-oil-drill-2.jpg" width="100%" height="250px"/>
                            </div>
                            <div class="col-md-3">
                                <img src="/images/vessels/thebigship.jpg" width="100%" height="250px"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <br>
                    </article>

                </div>

            </div>
        </div>
    </div>
</section>
@endsection