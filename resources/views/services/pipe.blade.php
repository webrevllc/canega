@extends('layout')

@section('content')
    <section class="inner-banner service">
        <div class="thm-container">
            {{--<h2 class="text-center">PIPE LAYING AND<br>PLATFORM INSTALLATION SERVICES</h2>--}}
            <h2 class="text-center">PLATFORM INSTALLATION AND<br> PIPE LAYING SERVICES</h2>
        </div>
    </section>
    <section class="blog-page single-post-page ">
    <div class="thm-container">
        <div class="row">
            <div class="col-md-12 pull-left">

                <div class="single-post-wrapper">
                    <article class="single-blog-post img-cap-effect">

                        <div class="row">
                            <div class="col-md-12" style="margin-top:50px;">

                                <div class="col-md-6">
                                    <h3 class="text-justify">
                                        Canega Offshore Services has signed exclusive partnerships agreements with Petro-Pride Subsea Limited, CNS Marine Nigeria and Grupo Protexa to jointly provide Pipe Laying and Platform Installation Services in West Africa and Nigeria.
                                        <br>
                                        <br>
                                        This joint venture has over 80 years combined experience in the Offshore Oil and Gas Industry assuring our customers receive efficient, personal and knowledgeable service of the highest quality.
                                    </h3>
                                </div>

                                <div class="col-md-6 ">
                                    <img src="/images/vessels/oilfields.png" width="100%" height="250px"/>
                                </div>
                             </div>
                         </div>
                        <div class="row" style="padding:20px;margin:20px;">
                            <div class="col-md-6 ">
                                <img src="/images/vessels/photo-17.JPG" width="100%" height="250px"/>
                            </div>
                            <div class="col-md-6">
                                <img src="/images/vessels/pins.png" width="100%" height="250px"/>
                            </div>
                        </div>
                        <div class="row" style="padding:20px;margin:20px;">
                            <div class="col-md-6 col-md-offset-3">
                                <img src="/images/vessels/pipelaying.png" width="100%" height="250px"/>
                            </div>

                            {{--<div class="col-md-3">--}}
                                {{--<img src="/images/vessels/customs/4839a.jpg" width="100%" height="200px"/>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-3">--}}
                                {{--<img src="/images/vessels/customs/projecthandling1.jpg" width="100%" height="200px"/>--}}
                            {{--</div>--}}
                        </div>
                    </article>

                </div>

            </div>
        </div>
    </div>
</section>
@endsection