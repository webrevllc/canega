@extends('layout')

@section('header')
    <style>
        .positioning-banner {
            margin-bottom: 50px;
            padding: 10px 20px;
            text-align: center;
            font-size: 22px;
            color: #fff;
            background: #1f4e79;
        }

        .title-area {
            font-weight: bold;
            color: #fff;
            padding: 10px;
            font-size: 20px;
        }

        .red {
            background: #d23028;
            background: -moz-linear-gradient(top, #d23028 0%, #8b1615 100%);
            background: -webkit-linear-gradient(top, #d23028 0%, #8b1615 100%);
            background: linear-gradient(to bottom, #d23028 0%, #8b1615 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#d23028', endColorstr='#8b1615', GradientType=0);
        }

        .green {
            background: #aad172;
            background: -moz-linear-gradient(top, #aad172 0%, #40a34e 100%);
            background: -webkit-linear-gradient(top, #aad172 0%, #40a34e 100%);
            background: linear-gradient(to bottom, #aad172 0%, #40a34e 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#aad172', endColorstr='#40a34e', GradientType=0);
        }

        .blue {
            background: #4593d0;
            background: -moz-linear-gradient(top, #4593d0 0%, #3a59a8 95%);
            background: -webkit-linear-gradient(top, #4593d0 0%, #3a59a8 95%);
            background: linear-gradient(to bottom, #4593d0 0%, #3a59a8 95%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#4593d0', endColorstr='#3a59a8', GradientType=0);
        }

        .row {
            margin-bottom: 50px;
        }
        .bb{margin-left:0px;}

    </style>
@endsection

@section('content')
    <section class="inner-banner service">
        <div class="thm-container">
            {{--<h2 class="text-center">PIPE LAYING AND<br>PLATFORM INSTALLATION SERVICES</h2>--}}
            <h2 class="text-center">POSITIONING, GEOPHYSICAL AND<br> GEOTECHNICAL SERVICES</h2>
        </div>
    </section>
    <section class="blog-page single-post-page ">
        <div class="thm-container">
            <div class="row">
                <div class="col-md-12" style="margin-top:50px;">
                    <h3 class="positioning-banner blue">Canega, in an exclusive partnership with Capital Signal, is offering the below listed services in Mexico to the Oil & Gas Industry.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <h3 class="title-area red">Positioning Services</h3>
                    <ul class="service-list">
                        <h3><i class="fa fa-circle bb"></i> As-built Survey</h3>
                        <h3><i class="fa fa-circle bb"></i> Rig Move & Vessel Positioning Service</h3>
                        <h3><i class="fa fa-circle bb"></i> Navigations/QC/Remote Tug Navigation</h3>
                        <h3><i class="fa fa-circle bb"></i> Template and Jacket Installation</h3>
                        <h3><i class="fa fa-circle bb"></i> Satellite Differential and RTK</h3>
                        <h3><i class="fa fa-circle bb"></i> Acoustic Positioning (USBL)</h3>
                        <h3><i class="fa fa-circle bb"></i> ROV Support Services</h3>
                        <h3><i class="fa fa-circle bb"></i> Pipeline Installation</h3>
                        <h3><i class="fa fa-circle bb"></i> Diver & Construction support services</h3>
                        <h3><i class="fa fa-circle bb"></i> Dimensional Control Surveys</h3>
                        <h3><i class="fa fa-circle bb"></i> Vessel Calibration Services – Gyro & DGPS</h3>
                        <h3><i class="fa fa-circle bb"></i> Hydrographic (water) and Topographic (land) Surveys</h3>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-6">
                    <img src="/images/services/geo1.jpg" width="100%"/>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <img src="/images/services/geo2.png" width="100%"/>
                </div>
                <div class="col-xs-12 col-md-4">
                    <h3 class="title-area blue">Geophysical Services</h3>
                    <ul class="service-list">
                        <h3><i class="fa fa-circle bb"></i> Pipeline/Cable route surveys</h3>
                        <h3><i class="fa fa-circle bb"></i> Site Hazard surveys</h3>
                        <h3><i class="fa fa-circle bb"></i> Debris Searches</h3>
                        <h3><i class="fa fa-circle bb"></i> Single & Multi-beam Surveys</h3>
                        <h3><i class="fa fa-circle bb"></i> Side Scan Sonar Surveys</h3>
                        <h3><i class="fa fa-circle bb"></i> Magnonetometer Surveys</h3>
                        <h3><i class="fa fa-circle bb"></i> Deep Water Multi-beam Bathymetric</h3>
                        <h3><i class="fa fa-circle bb"></i> Shallow Water Hazards</h3>
                        <h3><i class="fa fa-circle bb"></i> Hydrographic Surveys</h3>
                        <h3><i class="fa fa-circle bb"></i> Dredge QA/QC Consultancy</h3>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <h3 class="title-area green">Geotechnical Services</h3>
                        <h3><i class="fa fa-circle bb"></i> Marine/Offshore Site Investigations including Platform/Jackup</h3>
                        <h3><i class="fa fa-circle bb"></i> M.O.D.U. and Pipeline Route Investigations</h3>
                        <h3><i class="fa fa-circle bb"></i> Transition Zone and Onshore Site Investigations</h3>
                        <h3><i class="fa fa-circle bb"></i> Standard and Advanced Static Testing</h3>
                        <h3><i class="fa fa-circle bb"></i> M.O.D.U. (Jack-up) Footing Stability Analysis</h3>
                        <h3><i class="fa fa-circle bb"></i> Standard and Advanced Static Geotechinical Reporting</h3>
                        <h3><i class="fa fa-circle bb"></i> General Geotechnical Consultancy Services</h3>
                </div>
                <div class="col-xs-12 col-md-6">
                    <img src="/images/services/geo3.png" width="100%"/>
                </div>
            </div>
        </div>
    </section>
@endsection