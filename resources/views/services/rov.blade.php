@extends('layout')

@section('content')
    <section class="inner-banner service">
        <div class="thm-container">
            <h2 class="text-center">ROV INSPECTIONS</h2>
            <br>
        </div>
    </section>
    <section class="blog-page single-post-page " style="margin-top:50px;">
    <div class="thm-container">
        <div class="row">
            <div class="col-md-12 pull-left">
                <div class="single-post-wrapper">
                    <article class="single-blog-post img-cap-effect">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3" >
                                <h3 class="text-center">Canega Offshore Services in an exclusive partnership with ROVOP,
                                    will provide ROV Services in Mexico.
                                    These services will include:</h3>
                                    <br>
                                    <div class="text-left col-md-5 col-md-offset-4">
                                        <h3><i class="fa fa-circle bb"></i> Drill Support</h3>
                                        <h3><i class="fa fa-circle bb"></i> Construction</h3>
                                        <h3><i class="fa fa-circle bb"></i> IRM</h3>
                                        <h3><i class="fa fa-circle bb"></i> Survey</h3>
                                        <h3><i class="fa fa-circle bb"></i> Cable Lay Support</h3>
                                        <h3><i class="fa fa-circle bb"></i> Decommissioning</h3>
                                    </div>

                                    <div class="clearfix"></div>
                                <br>
                                    <h3 class="text-center">Some of the equipment in inventory is:</h3>
                                </h3>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12">
                                <div class="img-box">
                                    <img src="/images/services/rovimg.PNG" alt="Awesome Image"/>
                                </div>
                            </div>

                        </div>
                        <br>
                        <br>
                    </article>

                </div>

            </div>
        </div>
    </div>
</section>
@endsection