@extends('layout')

@section('content')
    <section class="inner-banner service">
        <div class="thm-container">
            <h2 class="text-center">SUPPLY CHAIN MANAGEMENT</h2>
            <br>
        </div>
    </section>
    <section class="blog-page single-post-page " style="margin-top:50px;">
        <div class="thm-container">
            <div class="row">
                <div class="col-md-12 pull-left">
                    <div class="single-post-wrapper">
                        <article class="single-blog-post img-cap-effect">
                            <div class="row">
                                <h3 class="text-justify">
                                    As part of our vessel acquisition
                                    program, Canega Group is
                                    presently developing the first
                                    Supply Chain Management
                                    System to be offered in Mexico as
                                    part of Mexico Energy Reform.
                                    The use of Canega Group PSV’s
                                    and FISV’s, will save International
                                    Oil Companies time and money
                                    in the customs clearance of the
                                    cargo as foreign flag vessels are
                                    not allowed to conduct cabotage.
                                </h3>
                                <br>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="img-box">
                                        <img src="/images/services/supplyimg.PNG" alt="Awesome Image" class="center-block" style="width:80%;"/>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </article>
                        <br>
                        <br>
                        <br>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection