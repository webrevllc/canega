@extends('layout')

@section('content')
    <section class="inner-banner service">
        <div class="thm-container">
            <h2 class="text-center">AGENCY DEPARTMENT <br> TANKER DIVISION</h2>
        </div>
    </section>
    <section class="blog-page single-post-page ">
    <div class="thm-container">
        <div class="row" style="margin-bottom:60px;">
            <div class="col-md-12">
                <div class="text-box text-center">
                    <h1 style="padding:30px;">Crude Oil, Clean Product & Chemical Tankers, LNG and LPG Carriers.</h1>
                </div>
                <div class="single-post-wrapper">
                    <article style="padding-top:40px;" >
                        <div class="row">
                                <h3>
                                    Canega offers a full range of agency services from cargo operations, bunkering, husbandry and global protective agency services. We manage both spot calls and contractual agreements for ship and offshore assets in over 35 countries and 100+ ports.
                                    <br>
                                    <br>
                                    Our agents, backed by more than 30 years of extensive agency experience and competence, provide the following services:
                                </h3>
                                <br>
                                <h3><i class="fa fa-circle bb"></i> <b>Full Port Agency</b> – Managing port and cargo operations as well as service procurement, reporting and accounting on behalf of ship owner and charterers.</h3>
                                <h3><i class="fa fa-circle bb"></i> <b>Protective Agency</b> – Representing ship owners’ and operators’ interests in port operations process when the charterer's nominated agent has been appointed.</h3>
                                <h3><i class="fa fa-circle bb"></i> <b>Husbandry Agency</b> – Managing service requirements for ship and crew, as well as bunker calls and transits.</h3>
                                <h3><i class="fa fa-circle bb"></i> <b>Cargo Agency </b>– Representing the cargo owners’ interests in the port either as the nominated agent or as the shippers/receivers agents for both wet and dry operations. Also involved in generating bill of lading.</h3>
                                <h3><i class="fa fa-circle bb"></i> <b>Crew support</b> – Arranging transport, accommodation and facilitating immigration.</h3>
                                <h3><i class="fa fa-circle bb"></i> <b>General Port Agency.</b></h3>
                                <h3><i class="fa fa-circle bb"></i> <b>Logistics Services</b> – Arranging air freight, warehousing, distribution and customs clearance for ships spares and crew.</h3>
                        </div>

                        <div class="row" style="margin-top:30px;">
                            <div class="col-md-6">
                                <a href="/images/vessels/tanker.jpg" data-lity>
                                    <img src="/images/vessels/tanker.jpg" height="250px" width="100%" alt="Awesome Image"/>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <a href="/images/vessels/japan-oil-tanker.jpg" data-lity>
                                    <img src="/images/vessels/japan-oil-tanker.jpg" height="250px" width="100%" alt="Awesome Image"/>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="/images/vessels/tanker2.jpg" data-lity>
                                    <img src="/images/vessels/tanker2.jpg" height="250px" width="100%" alt="Awesome Image"/>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <a href="/images/vessels/orig_105832.jpg" data-lity>
                                    <img src="/images/vessels/orig_105832.jpg" height="250px" width="100%" alt="Awesome Image"/>
                                </a>
                            </div>

                        {{--<br>--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-4">--}}
                                {{--<a href="/images/vessels/tanker.jpg" data-lity>--}}
                                    {{--<img src="/images/vessels/tanker.jpg" height="200px" width="100%" alt="Awesome Image"/>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-4">--}}
                                {{--<a href="/images/vessels/japan-oil-tanker.jpg" data-lity>--}}
                                    {{--<img src="/images/vessels/japan-oil-tanker.jpg" height="200px" width="100%" alt="Awesome Image"/>--}}
                                {{--</a>--}}

                            {{--</div>--}}
                            {{--<div class="col-md-4">--}}
                                {{--<a href="/images/vessels/tanker2.jpg" data-lity>--}}
                                    {{--<img src="/images/vessels/tanker2.jpg" height="200px" width="100%" alt="Awesome Image"/>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="row">
                            <h3 class="text-center" style="padding-top:30px;">

                            </h3>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection