<ul>
    <li class="{{ set_active(['services/sacv']) }}" ><a href="/services/sacv" >CANEGA S.A. DE C.V. </a></li>
    <li class="{{ set_active(['services/shipping']) }}"><a href="/services/shipping">CANEGA Shipping Services</a></li>
    <li class="{{ set_active(['services/offshore']) }}"><a href="/services/offshore">CANEGA Offshore Services</a></li>
    <li class="{{ set_active(['services/shipping-nv']) }}"><a href="/services/shipping-nv" >Canega Shipping Services N.V.</a></li>
    <li class="{{ set_active(['services/internacional']) }}"><a href="/services/internacional" >Canega Internacional</a></li>
    <li class="{{ set_active(['services/fze']) }}"><a href="/services/fze" >Canega Trading FZE</a></li>
    <li class="{{ set_active(['services/singapore']) }}"><a href="/services/singapore" >CANEGA INTERNATIONAL, PTE LTD</a></li>
</ul>