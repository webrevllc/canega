@extends('layout')

@section('content')


    <section class="inner-banner about">
        <div class="thm-container">
            <h2 class="text-center" style="text-shadow: 1px 2px 4px #000">Testimonials</h2>
            {{--<ul class="breadcumb">--}}
                {{--<li><a href="/"><i class="fa fa-home"></i> Home</a></li>--}}
                {{--<li><span>Our Policies</span></li>--}}
            {{--</ul>--}}
        </div>
    </section>
    <section class="sec-padding">
        <div class="thm-container">
            <div class="row">
                <div class="col-md-12 pull-right">
                    {{--<div class="sec-title">--}}
                        {{--<h2><span>What our client’s says</span></h2>--}}
                    {{--</div>--}}
                    <table class="table table-bordered">
                        <tr>
                            <td class="col-md-1"><img src="{{asset('assets/heerema.png')}}" width="100%" class="center-block"/></td>
                            <td class="col-md-10" style="vertical-align: middle;">
                                <h3>Heerema Marine Contractors</h3>
                            </td>
                            <td class="col-md-1"><a href="{{asset('pdfs/letters/heerema.pdf')}}" style="margin-top:15px;" class="btn btn-block btn-info" target="_blank"><i class="fa fa-eye"></i>   View</a></td>
                        </tr>
                        <tr>
                            <td class="col-md-1"><img src="{{asset('assets/tms.jpg')}}" width="100%" class="center-block"/></td>
                            <td class="col-md-10" style="vertical-align: middle;">
                                <h3>Tidewater de M&eacute;xico</h3>
                            </td>
                            <td class="col-md-1"><a href="{{asset('pdfs/letters/tms.pdf')}}" style="margin-top:15px;" class="btn btn-block btn-info" target="_blank"><i class="fa fa-eye"></i>   View</a></td>
                        </tr>
                        <tr>
                            <td class="col-md-1"><img src="{{asset('assets/solstad.jpg')}}" width="100%" class="center-block"/></td>
                            <td class="col-md-10" style="vertical-align: middle;">
                                <h3>Solstad Offshore</h3>
                            </td>
                            <td class="col-md-1"><a href="{{asset('pdfs/letters/Solstad.pdf')}}" style="margin-top:15px;" class="btn btn-block btn-info" target="_blank"><i class="fa fa-eye"></i>   View</a></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>

@endsection