@extends('layout')

@section('content')
    <section class="sec-padding blog-page">
        <div class="thm-container">
            <div class="row">
                <div class="col-md-4 pull-left">
                    <div class="single-sidebar-widget">
                        <div class="sec-title">
                            <h2><span>Vessels</span></h2>
                        </div>
                        <div class="categories">
                            <ul>
                                @foreach($types as $t)
                                    <li><a href="/fleet/{{$t->id}}">{{$t->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 pull-right">
                    <div class="single-post-wrapper">
                        @foreach($vessels as $v)
                            <article class="single-blog-post img-cap-effect">
                                <div class="img-box">
                                    <img src="{{$v->image_url}}" alt="Awesome Image"/>
                                </div>
                                <div class="meta-info">
                                    <div class="date-box">
                                        <div class="inner-box">
                                            <b>{{date('d', strtotime($v->created_at))}}</b>
                                            {{date('M', strtotime($v->created_at))}}
                                        </div>
                                    </div>
                                    <div class="content-box">
                                        <h3>{{$v->name}}</h3>
                                        <ul class="post-links">
                                            <li><a href="#"><i class="fa fa-calendar"></i> Built: {{$v->built}}</a></li>
                                            <li><a href="#"><i class="fa fa-anchor"></i> Class: {{$v->class}}</a></li>
                                            @if($v->id ==1 || $v->id == 2)
                                                <li><a href="#"><i class="fa fa-bed"></i> Beds: {{$v->beds}}</a></li>
                                            @else
                                                <li><a href="#"><i class="fa fa-arrows-h"></i> LOA: {{$v->loa}}</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <table class="table-bordered table">
                                    <thead>
                                        <th>Name</th>
                                        <th>Built</th>
                                        <th>Class</th>
                                        @if($v->id ==1 || $v->id == 2)
                                            <th>Crane</th>
                                            <th>Mooring</th>
                                         @else
                                            <th>Pax</th>
                                            <th>Speed</th>
                                         @endif
                                        <th>Fire Fighting</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$v->name}}</td>
                                            <td>{{$v->built}}</td>
                                            <td>{{$v->class}}</td>
                                            @if($v->id ==1 || $v->id == 2)
                                                <td>{{$v->crane}}</td>
                                                <td>{{$v->mooring}}</td>
                                            @else
                                                <td>{{$v->pax}}</td>
                                                <td>{{$v->speed}}</td>
                                            @endif

                                            <td>{{$v->firefighting}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <a href="{{$v->pdf_url}}"  target="_blank" class="read-more">View PDF</a>
                            </article>
                        @endforeach


                    </div>

                    {{--<ul class="page-navigation text-left">--}}
                        {{--<li><span>1</span></li>--}}
                        {{--<li><a href="#">2</a></li>--}}
                        {{--<li><a href="#"><i class="fa fa-long-arrow-right"></i></a></li>--}}
                    {{--</ul>--}}

                </div>
            </div>
        </div>
    </section>

@endsection